#! /bin/env bash

set -e

KOBO_DATA_D="./data/kobo"

if [[ ! -f "config.yaml" ]]
then
  echo "Run this script from the root of a Hugo project."
  exit 1
fi

if [[ ! -d "$KOBO_DATA_D" ]]
then
  echo "Create a $KOBO_DATA_D directory first."
  exit 1
fi


# HACK Overwrite the file (ab)using the -N/--timestamping flag
wget \
  --timestamping \
  --directory-prefix="$KOBO_DATA_D" \
  https://gitlab.com/Roboe/kobo-offline/-/raw/master/data/kobo/devices.json

wget \
  --timestamping \
  --directory-prefix="$KOBO_DATA_D" \
  https://gitlab.com/Roboe/kobo-offline/-/raw/master/data/kobo/dictionaries.json
