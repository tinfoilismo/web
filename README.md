# Tinfoilismo

> **Esta guía está inacabada**. Estamos trabajando en ella.

## Introducción técnica

[Tinfoilismo](https://tinfoilismo.org) está hecho con el generador de sitios estáticos [Hugo](https://gohugo.io/) y se hospeda en un [repositorio de GitLab](https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io). Esto significa que no tiene una base de datos para guardar el contenido, como sería el caso de WordPress y otros gestores, sino que las páginas se crean utilizando [ficheros de texto](content/). Cada fichero tiene unos metadatos y un contenido escrito en el lenguaje de marcado Markdown. Cada vez que se añade o edita una página, el sitio vuelve a regenerarse, y como resultado arroja un montón de archivos estáticos HTML y CSS. Estos archivos conforman el sitio web.

Para publicar estos archivos, utilizamos el sistema de [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). Como nuestro sitio tiene que generarse, utilizamos también el sistema de despliegue continuo de GitLab, llamado [GitLab CI](https://docs.gitlab.com/ee/ci/). Cada vez que se hace un commit, un cambio a los ficheros, y se sube al repositorio mediante Git, este sistema [regenera automáticamente el sitio con Hugo](.gitlab-ci.yml) y lo publica. De esa manera integramos todo el flujo en los servidores de GitLab.

A continuación se explica cómo añadir o editar un artículo. Si quieres saber cómo generar el sitio de manera manual, [lo puedes ver más abajo](#instalación-manual).


## Guía de colaboración

> **Por el momento, es necesario pertenecer al equipo de Tinfoilismo para poder hacer cambios**. Aún estamos preparando todo el terreno, pero en cuanto lo hagamos abriremos esta posibilidad.

Para añadir o editar un nuevo artículo basta con añadir o editar el fichero Markdown correspondiente.

### En el gestor de contenidos Netlify CMS (no disponible para artículos)

Se puede acceder a un editor web en la dirección https://tinfoilismo.org/editor/. Desde ahí, después de identificarnos con nuestra cuenta de GitLab, podemos editar los sucesos de la cronología y anuncios de meta existentes o añadir nuevos de manera muy cómoda.

Una pequeña pega: si en el selector de `sujetos` no encuentras el correspondiente al suceso, debes dejarlo en blanco. Después de publicar el suceso, tendrás que añadir ese parámetro manualmente con cualquiera de los métodos descritos a continuación.

### Directamente en la web de GitLab

En el apartado de Meta de cada artículo publicado hay un enlace al archivo Markdown correspondiente en GitLab. Podemos editar los contenidos directamente allí con el editor web de GitLab si contamos con una cuenta de GitLab. Si estamos identificados y tenemos permisos para editar el contenido, basta con pulsar en el botón `Edit`.

Puedes aprender cómo escribir en Markdown en el [tutorial de Commonmark](https://commonmark.org/help/).

### Mediante un editor en tu ordenador

Para contribuír de esta manera, debes seguir los pasos de la instalación manual descritos a continuación y saber utilizar Git para subir tus cambios al repositorio de GitLab.


## Instalación manual

Podemos optar por descargar el sitio completo y generar y servir los estáticos en local. Para ello se requiere tener instalado:

- [Hugo](https://gohugo.io/about/) v0.79 o superior
- [Git](https://git-scm.com/)

Hugo se puede instalar siguiendo cualquier método de la [guía de instalación de Hugo](https://gohugo.io/getting-started/installing). Puedes econtrar aquí [la última versión de los instaladores](https://github.com/gohugoio/hugo/releases/latest).

### En Debian

Aunque el repositorio de Debian 10 (Buster) dispone de una version de Hugo, es una versión demasiado desfasada. Podemos obtener una versión más actualizada si usamos el [repositorio de _backports_ de Debian](https://backports.debian.org/).

No recomendamos este método para desarrollar el tema, puesto que el despliegue continuo usará siempre la última versión disponible de Hugo y puede causar incompatibilidades.

Podemos añadir el repositorio de _backports_ ejecutando lo siguiente como superusuario:

    echo "deb http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list

Y para instalar el paquete de Hugo:

    apt-get update
    apt-get -t buster-backports install hugo

    hugo version


### Generar el sitio web

Para generar una compilación estática (de producción):

    hugo -v

Encontrarás los archivos en la carpeta `build`.


### Servir el sitio web

Necesitas clonar o actualizar el proyecto:

    # Mediante SSH
    git clone git@gitlab.com:tinfoilismo/tinfoilismo.gitlab.io.git

    # Mediante HTTPS
    git clone https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io.git

    cd tinfoilismo.gitlab.io

Puedes visualizar el contenido del proyecto con el servidor de desarrollo de Hugo. La opción `--buildDrafts` o `-D` incluye también los borradores no publicados:

    hugo serve -D

Ahora abre http://localhost:1313 en un navegador y podrás navegar por el contenido.

Puedes hacer cambios en los ficheros del proyecto con tu editor favorito. Si tu editor tiene previsualización de Markdown, asegúrate de que sigue el estándar de Commonmark. Según guardes los ficheros, el sitio web se regenerará y la página abierta en el navegador se recargará automáticamente.

Cuando hayas terminado de editar o añadir contenido, haz un commit con los cambios que quieras subir. Es importante hacer un cambio por cada artículo y utilizar la nomenclatura adecuada en la descripción del commit.
