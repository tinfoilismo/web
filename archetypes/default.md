---
title: "{{ replace .Name "-" " " | title }}"
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/NÚMERO
summary: Sumario muy cortito, de unos 140 caracteres como máximo, para mostrar en los listados.
etiquetas:
  - eje-apertura
  - eje-descentralizacion
  - eje-inclusion
  - eje-privacidad-seguridad
image:
  - imagen-principal.jpg
draft: true
---

Ver todos los posibles elementos de marcado en el archivo **MARKDOWN.md**.

Se pueden usar títulos de segundo, tercer y cuarto nivel. No se debe usar la sección de primer nivel, ya que la cabecera h1 está reservada para el título del artículo.


// Obra de arte u otro artefacto multimedia que tenga la virtud de hacer entender intuitivamente lo que se pretende ilustrar con el artículo.

![Texto alternativo](imagen.jpg "Título de la imagen, que se mostrará como pie de foto y al poner el puntero encima de la imagen. Aquí se puede decir también quién es el autor.")

La imagen especificada en la cabecera será la que se verá cuando compartamos el enlace (ver [OpenGraph](https://ogp.me/)).


Texto del artículo, con referencias,[^1] cuando sea necesario.


## Píldoras informativas

### Vídeos/Infografías/Artículos divulgativos

**D de mes del 20xx**. Resumen corto. _Título original_ (editor). https://enlace.tld


## Para profundizar

### Libros/Documentales

_Título original_, autor, año. https://enlace.tld


## Referencias

[^1]: _Título original_. https://enlace.tld
