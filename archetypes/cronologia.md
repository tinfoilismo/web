---
headless: true
resumen: "{{ replace .Name "-" " " | title }}"
fecha: 20YY-MM-DD
temas:
{{- range $key, $value := .Site.Taxonomies.temas }}
  - {{ $key -}}
{{ end }}
sujetos:
{{- range $key, $value := .Site.Taxonomies.sujetos }}
  - {{ $key -}}
{{ end }}
---

El nombre de este archivo ha de ser de tipo **2021-01-31-lorem-ipsum.md** para poder extraer la fecha.

Resumen corto. [Fuente]() &middot; [Otra fuente]()

  - **[día] de [mes].** Respuesta oficial o reacción. [Fuente]()
