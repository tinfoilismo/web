---
title: Capitalismo de vigilancia
featured: true
etiquetas:
  - eje-inclusion
  - eje-privacidad-seguridad
aliases:
  - /docs/alfabetización/capitalismo-de-vigilancia
  - /docs/alfabetización/big-data
  - /wiki/big-data/
  - /wiki/capitalismo-de-vigilancia/
---

![Dos aparentes cargadores de teléfono móvil, uno negro y otro blanco, situados en paralelo. A la derecha, las siglas «FANGo», compuestas por la inicial del logo de Facebook, Amazon, Netflix y Google, respectivamente, y el símbolo O con barra (Ø)](fango.jpg "Producto artístico [«FANGo»](http://martinnadal.eu/fango/), del programador creativo y artista Martin Nadal.\
«FANGo» es un arma de defensa contra el capitalismo de vigilancia. Oculto bajo su apariencia de cargador de teléfono móvil, contiene un microcontrolador que toma el control del teléfono inteligente enchufado para realizar consultas a Google, Amazon y otros buscadores o lanzar vídeos en YouTube con el fin de engañar a los _data brokers_ en su actividad de captura de datos.\
Facebook, Amazon, Netflix y Google fueron agrupados bajo la sigla «FANG» como las empresas más importantes y de alto rendimiento del NASDAQ, pioneras de lo que hoy se conoce como capitalismo de vigilancia.")


El **capitalismo de vigilancia** es un modelo de negocio basado en la recolección, análisis y aprovechamiento de los datos personales de las personas[^1] en lo que se conoce como «perfiles de datos».

Tu perfil de datos es más una parodia de ti que un reflejo fidedigno: es una reducción numérica, una cuantificación de tu persona. Las compañías toman decisiones confiando en esos datos sesgados que no tienen por qué beneficiarte.[^2]


## Data brokers

Las compañías especializadas en recoger, agrupar y analizar datos de las personas se conocen como _data brokers_. Los recopilan de fuentes muy distintas, como tu navegación online, las transacciones de tarjetas bancarias, diversos registros o fuentes públicas. Después, crean perfiles sobre estos datos personales y listas segmentadas que pueden ser usados con distintos fines, como son los fines comerciales.[^3]

En muchos casos, estas compañías obtienen los datos de otros _data brokers_. Esto genera un efecto [matrioshka](https://es.wikipedia.org/wiki/Matrioshka) que dificulta conocer de qué fuentes en concreto han obtenido los datos, cómo los derivan o cómo se intercambian esos datos en el ecosistema mercantil.[^4] Esto, a su vez, dificulta tareas de vigilancia sobre el cumplimiento de las leyes de protección de datos o de abusos empresariales, por ejemplo.

Algunos tipos de _data brokers_ especializadas son:
- Comercializadores y anunciantes: Estas compañías, expertas en _marketing_, crean y publican anuncios, ofertas y contenidos digitales mediante los cuales obtienen información sobre tus actividades o sobre ti.
- Rastreadores (_trackers_): Estas compañías están virtualmente presentes en una gran cantidad de sitios web que visitas o aplicaciones que utilizas. Para ello, se introducen en ellos _widgets_, píxeles invisibles u otras técnicas avanzadas de identificación. Así, pueden unir los puntos de tu «huella digital» y deducir tu historial de navegación o tu historial de uso de aplicaciones móviles, por ejemplo.
- Analistas de aplicaciones (_analytics_): Estas compañías recopilan datos sobre tu actividad dentro de aplicaciones web o móviles. Para ello, se introduce bibliotecas de código en aplicaciones móviles o web. Así, recopilan los horarios en los que las utilizas, el tiempo que permaneces en ellas o cómo reaccionas a ciertos estímulos, por ejemplo.


## Gigantes de la vigilancia

El informe [_Gigantes de la vigilancia: La amenaza que el modelo de negocios de Google y Facebook representa para los derechos humanos_](https://www.amnesty.org/en/documents/pol30/1404/2019/es/), realizado por Amnistía Internacional y publicado en noviembre de 2019, describe las implicaciones para los derechos humanos del modelo empresarial basado en la vigilancia que sustenta a Internet, centrándose en dos empresas: Google y Facebook.

En primer lugar, se expone cómo dos empresas, Google y Facebook, han sido pioneras en el capitalismo de vigilancia. Al hacerlo han establecido entre ellos un dominio casi total sobre los canales primarios a través de los cuales las personas se conectan y se comprometen con el mundo en línea, y acceden y comparten información en línea, erigiéndose en los guardianes de la _plaza pública_ para gran parte de la humanidad. Esto les da una ventaja sin precedentes en la historia del poder de las empresas para influir en el disfrute de los derechos humanos.


## Píldoras informativas

### Vídeos

**1 de febrero del 2019**. Cuando el crecimiento económico llega a sus límites: la minería de datos. _Who is getting rich off you? The insidious big data economy_. https://bigthink.com/videos/who-is-getting-rich-off-you-the-insidious-big-data-economy


## Infografías

**12 de marzo del 2018**. Eticas Foundation, _Discriminación algorítmica. El reto de los datos en el siglo XXI_. https://eticasfoundation.org/es/algorithmic-discrimination-one-of-the-main-challenges-for-social-progress-in-the-21st-century/


## Referencias

[^1]: _La vigilancia generalizada de Facebook y Google representa un peligro sin precedente para los derechos humanos_, Amnistía Internacional. https://www.es.amnesty.org/en-que-estamos/noticias/noticia/articulo/la-vigilancia-generalizada-de-facebook-y-google-representa-un-peligro-sin-precedente-para-los-derech/

[^2]: _Tu identidad digital tiene tres capas y solo puedes proteger una de ellas_. https://qz.com/1525661/your-digital-identity-has-three-layers-and-you-can-only-protect-one-of-them/

[^3]: _¿Qué son los data brokers?_, Maldita Tecnología. https://maldita.es/malditatecnologia/2020/03/27/que-son-los-data-brokers/

[^4]: _How do data companies get our data?_, Privacy International. https://privacyinternational.org/long-read/2048/how-do-data-companies-get-our-data
