---
title: Inteligencia artificial
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/37
draft: true
---

_Tecnologías de reconocimiento facial: por qué tienen tantos problemas con la discriminación de personas con la piel oscura o negra_, Maldita Tecnología: https://maldita.es/malditatecnologia/2020/06/15/reconocimiento-facial-problemas-discriminacion-personas-piel-oscura-negra/

_¿Qué son las redes neuronales?_, Maldita Tecnología: https://maldita.es/malditatecnologia/2020/06/19/que-son-las-redes-neuronales/
