---
title: Economía de la atención
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/33
draft: true
---

<figure>

> Luego de que los dos hombres se inclinen ante él, Mr. Smith pasa a la sala contigua, una vasta galería de más de 3200 pies de longitud, consagrada a la publicidad atmosférica. Todos conocen esos enormes anuncios reflejados en las nubes, tan grandes que pueden ser vistos por poblaciones de ciudades e incluso de países enteros. [...]
>
> —¡Sí —murmura Mr. Smith—, un cielo sin nubes! Es lamentable, pero ¿qué podemos hacer? ¿Producir lluvia? Podríamos, pero ¿con qué objeto? Lo que necesitamos son nubes, no lluvia. —Y dirigiéndose al ingeniero en jefe, le ordena—: Vaya a ver a Mr. Samuel Mark, de la división meteorológica del departamento científico y dígale de mi parte que se ponga a trabajar seriamente en el asunto de las nubes artificiales. ¡Nunca más estaremos a merced de los cielos despejados!

<figcaption><cite>La jornada de un periodista americano en 2889</cite>, Michel Verne.</figcaption>
</figure>


## Píldoras informativas

### Artículos de divulgación

**26 de junio de 2020**. _Homescapes y otros llamamientos a juegos gratuitos en redes sociales que te enganchan a ti y a tus datos_ (Maldita Tecnología). https://maldita.es/malditatecnologia/2020/06/26/juegos-gratuitos-anuncios-redes-sociales-enganchan-datos/


## Para profundizar

### Libros

_La civilización de la memoria de pez: pequeño tratado sobre el mercado de la atención_, Bruno Patino, 2020. https://www.alianzaeditorial.es/libro/alianza-ensayo/la-civilizacion-de-la-memoria-de-pez-bruno-patino-9788491819684/
