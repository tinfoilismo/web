---
title: Censura y filtros automáticos
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/36
draft: true
---

La censura indiscriminada y automatizada del sexo en internet produce daños colaterales a la libertad sexual, especialmente a las personas del colectivo LGTBI+, y degrada el sexo hacia el oscurantismo y la clandestinidad porque obstaculiza también a las comunidades que promueven actitudes positivas hacia el sexo. _Cómo la censura sexual terminó con el internet que amábamos_ (Engadget - B@d P@ssw0rd). https://www.engadget.com/2019/01/31/sex-censorship-killed-internet-fosta-sesta/

Bloqueo de pezones en Facebook y la imposible tarea de detectar el discurso de odio. https://maldita.es/malditatecnologia/2020/06/26/facebook-pezones-mujeres-inteligencia-artificial/

Tartas a prueba de propiedad intelectual. https://www.fabianmosele.com/copyright-proof-cakes
