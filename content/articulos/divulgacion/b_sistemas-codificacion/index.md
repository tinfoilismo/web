---
title: Sistemas de codificación
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/61
draft: true
---

A la hora de

## Numeración

Si alguien nos preguntara qué significa "11", lo primero que nos viene a la cabeza es una cierta cantidad que todos reconocemos y podemos contar fácilmente. Si usamos los dedos, serían dos manos enteras más un dedo. Si contamos círculos, entonces "11" círculos serían esta cantidad:

⬤⬤⬤⬤⬤⬤⬤⬤⬤⬤⬤

Pero, ¿y si dijéramos que "11" círculos también pueden ser esta cantidad?

⬤⬤⬤

No es un error. Cuando damos la primera respuesta, estamos asumiendo algo de forma implícita, y es que estamos contando en base 10, esto es, usando el sistema decimal. Pero si no especificamos la base, la segunda respuesta también puede ser correcta.

Hagamos el ejercicio de disociar el significante del significado. En el caso anterior, el significante es la grafía "11" y el significado la cantidad denotada por el número de círculos, que será una u otra dependiendo del contexto, es decir, la base.

¿Por qué? Porque en el sistema binario solo disponemos de dos grafías simples, que llamamos bits, para determinar cualquier cantidad (0 y 1), lo cual hace que cuando queramos contar cero o un elemento, todo siga siendo igual. Pero para contar dos elementos, ya necesitaremos dos cifras (10), y para contar tres elementos, tendremos que incrementar la última cifra (11). Para cuatro ya necesitamos añadir una tercera cifra y volver a empezar con las otras dos (100). Y así sucesivamente.

Pero esto no termina aquí, hay infinitas bases de numeración posibles, ya que podemos usar cualquier número como base para contar. La base 10 es solo una más. Aquí van algunos ejemplos:

11<sub>2</sub> → 3<sub>10</sub>

11<sub>3</sub> → 4<sub>10</sub>

11<sub>8</sub> → 9<sub>10</sub>

11<sub>10</sub> → 11<sub>10</sub>

11<sub>16</sub> → 17<sub>10</sub>

Dependiendo del subíndice (que denota la base), la grafía "11" puede denotar cantidades muy dispares.

Aquí vemos como la grafía "11", que creíamos fuertemente asociada a una cierta cantidad, en realidad puede representar diferentes cantidades. Solo tenemos que cambiar la base, que es el subíndice que acompaña a cada número. De este modo sí quedaría determinada la cantidad inequívocamente.

Esto es análogo a los idiomas humanos. Veamos un ejemplo: pensemos en la palabra "chat", que asociamos inmediatamente a "coversación". Pero... no vayamos ran rápido. Hemos asumido implícitamente que el idioma de contexto de esa palabra es el inglés. ¿Qué ocurre en francés? Resulta que en francés la grafía "chat" significa "gato" en español. Ocurre lo mismo con los sistemas numerales.

Una cantidad es un elemento natural, pero la grafía con la que la denotamos es algo cultural. Pensemos que los romanos usaban unas grafías totalmente diferentes a las nuestras para contar, aunque compartían con nosotros el uso de la base 10. Y han existido a lo largo de la historia muchos otros sistemas de numeración, desarrollados por diferentes culturas y civilizaciones. La cuestión es que contar en base 10 no es más natural que usar cualquier otra base, sino que es convención pura y dura.

Probablemente el motivo por el que se impuso la base 10 es simple y llanamente que tenemos 10 dedos en las manos, aunque indagar en esto probablemente nos llevaría a otros mundos históricos, filosóficos y psicológicos mucho más extensos, aunque seguramente apasionantes.

La cuestión es que, estrictamente hablando, podemos contar en cualquier base. Ejemplo, usemos la base 5:

0<sub>10</sub> → 0<sub>5</sub>

1<sub>10</sub> → 1<sub>5</sub>

2<sub>10</sub> → 2<sub>5</sub>

3<sub>10</sub> → 3<sub>5</sub>

4<sub>10</sub> → 4<sub>5</sub>

5<sub>10</sub> → 10<sub>5</sub>

6<sub>10</sub> → 11<sub>5</sub>

7<sub>10</sub> → 12<sub>5</sub>

Pero hoy en día hay algunas bases que se usan mucho más que otras, por diversos motivos. Las más habituales son:

- El binario es el lenguage de la electrónica y los ordenadores. El motivo es que los chips están construidos esencialmente con transistores, que son dispositivos electrónicos minúsculos que actúan como interruptores: o dejan pasar la corriente, o no la dejan pasar. Es decir, tienen dos estados. Con lo cual solo podemos codificar su estado con dos grafías, que son 0 y 1.

- El hexadecimal tiene que ver con el binario, y es que, a efectos de conversión, podemos tomar todos los bits de 4 en 4, y cada uno de estos segmentos se podría codificar con un solo dígito en hexadecimal. Para ello se necesitan grafías adicionales. Del 0 al 9 son las mismas que en decimal, pero de 10 a 15 se corresponden con las letras A-F del alfabeto latino (da igual mayúsculas que minúsculas).

  248<sub>10</sub> → F8<sub>16</sub> → 11111000<sub>2</sub> (F<sub>16</sub> → 1111<sub>2</sub>, 8<sub>16</sub> → 1000<sub>2</sub>)

  Como vemos, la conversión de una base a otra es muy fácil, ya que se puede hacer por segmentos, y cualquier ristra de bits reduce su tamaño a la cuarta parte, mucho más compacto y cómodo para los humanos. Concretamente un byte son ocho bits, pero solo son dos caracteres hexadecimales.

  Como el hexadecimal está muy extendido, existe una nomenclatura propia para indicar que una cifra es hexadecimal, que consiste en colocar "0x" al inicio.

  F8<sub>16</sub> = 0xF8

- El octal funcionaría igual, pero en lugar de dividir el código binario en segmentos de 4 bits, lo haríamos en segmentos de 3 bits.

  42<sub>10</sub> → 52<sub>8</sub> → 101010<sub>2</sub> (5<sub>8</sub> → 101<sub>2</sub>, 2<sub>8</sub> → 010<sub>2</sub>)

- El ternario se puede asociar a ciertos tipos de electrónica analógica, como por ejemplo, los transistores de tipo CMOS, que pueden estar en tres estados diferentes, que son: en alta, en baja (a tierra) o abierto (en alta impedancia). También se asocia a que los estados en computación cuántica podrían expresarse de forma ternaria (siendo el tercer estado una superposición de los otros dos estados), aunque esto es una simplificación muy burda de algo mucho más complejo.

Es decir, que el sistema decimal, tan natural para los humanos, resulta que no lo es tanto para la informática.

https://twitter.com/bagder/status/1389605985422036994

### En Python 3

A la hora de trabajar con números enteros, podemos especificar un número y su base para obtener su representación en base decimal:

```python
>>> int('11')
11
>>> int('11', base=10)  # La base 10 es ya la base por defecto
11
>>> int('11', base=2)   # Binario
3
>>> int('11', base=8)   # Octal
9
>>> int('11', base=16)  # Hexadecimal
17
```

Mismo proceso, con diferente nomenclatura:

```python
>>> 11
11
>>> 0b11  # Literal binario
3
>>> 0o11  # Literal octal
9
>>> 0x11  # Literal hexadecimal
17
```

Para obtener la representación en otras bases de un número decimal:

```python
>>> bin(3)
'0b11'
>>> oct(9)
'0o11'
>>> hex(17)
'0x11'
```

## Caracteres

Para cualquier tipo de comunicación, si miramos a bajo nivel, el sistema binario resulta mucho más natural. Pensemos en el telégrafo y en el código Morse, en el cual existen dos niveles de potencia, uno de los cuales consiste en un circuito cerrado (lo que podemos asociar de forma arbitraria con un uno), y el otro es un circuito abierto (que podemos asociar con un cero por descarte). El circuito cerrado transmite potencia electromagnética, y el abierto, no. El ritmo de estas aperturas y cierres es lo que da lugar a una codificación, que interpretada de igual forma por emisor y receptor es lo que da lugar a la información.

Traslademos esto a sistemas mucho más modernos, como la fibra óptica, en la cual un haz de luz se puede interpretar por ejemplo como un 1, y la ausencia de ese haz, como un 0. Es algo totalmente arbitrario en lo que se tendrán que poner de acuerdo los dispositivos conectados.

En realidad tenemos un alfabeto que también necesitamos codificar para comunicarnos. Y muchos otros símbolos. Pero hemos visto que lo más sencillo y eficiente es transmitir únicamente ceros y unos. ¿Cómo es posible entonces que podamos leer mensajes de texto? Y no solo en caracteres latinos, sino en cualquier alfabeto o tipo de escritura.

Pues bien, para esto necesitamos una forma de relacionar un carácter con un código, que a su vez pueda convertirse en un fenómeno físico sencillo y robusto, es decir, algo fácilmente transmitible e interpretable, con una cantidad mínima de errores. En Morse, por ejemplo, se determinó que los cierres de corriente antes mencionados tuvieran una interpretación, de modo que un cierre "corto" es un "punto", y un cierre "largo" es una "raya". Y a su vez, se codificó cada carácter con una combinación de puntos y rayas. De ahí por ejemplo el famoso mensaje "SOS", donde una "S" se codifica con tres puntos, y una "O" con tres rayas.

En el mundo de la informática esto ha de hacerse de forma binaria, como hemos aprendido. Es decir, mediante una combinación de unos y ceros. El primer sistema que se desarrolló con este fin fue ASCII (*American Standard Code for Information Interchange*), que lo que hace es codificar un juego de caracteres básico en 7 bits (2⁷ = 128 combinaciones posibles), en el que se incluyen los números, las letras mayúsculas y minúsculas del alfabeto inglés, signos de puntuación y algunos caracteres especiales. A cada uno de ellos se le asignó un número entre 32 y 127. Por debajo de 32 se asignan a caracteres de control no imprimibles (como por ejemplo un tabulador, un retorno de carro, etc), algunos de los cuales ya han perdido su utilidad, pero eran necesarios en otros tiempos, y se han mantenido por razones históricas y de compatibilidad hacia atrás.

Poner aquí tabla.

Ejemplo

```
01100010 01101001 01110100 01110011
98       105      116      115
b        i        t        s
```

Pero en un byte caben 8 bits, lo cual provocó que durante una época las restantes 128 posiciones se intentaran identificar con multitud de caracteres, dependiendo del contexto o el idioma de cada cual, dando lugar a diferentes extensiones (de hecho no existe como tal un ASCII extendido, sino muchos). Las extensiones más populares fueron la familia de códigos ISO 8859, que reservaba los primeros 128 caracteres para ASCII, iguales para todo el mundo, pero los últimos 128 caracteres dieron lugar a multitud de codificaciones. Por ejemplo, en España era necesario incluir letras como "ñ", "ç" o vocales con tilde. Los hablantes de griego o hebreo necesitaban incluir sus propios caracteres, igual que los que usan el alfabeto cirílico. Cada uno de estos códigos contiene los caracteres necesarios para un idioma o un determinado conjunto de idiomas.

Pero esto provocó multitud de problemas, ya que evidentemente un documento que hubiera sido escrito en griego, por ejemplo, no podía leerse en un ordenador cuya codificación por defecto fuera la española, por ejemplo, sino que obligaría al receptor a cambiar el código de su máquina o su aplicación para poder interpretarlo correctamente. Con lo cual era necesario especificar, junto al texto transmitido, el código con el que se había codificado.

Por otro lado, a veces era imposible escribir un único documento en dos idiomas diferentes. Por ejemplo, si necesitáramos redactar un documento en español y en griego, necesitaríamos caracteres procedentes de ISO 8859-1 e ISO 8859-7, que además se superponen entre sí. La posición 0xF1 está ocupada por la "ñ" en el primero y la "ρ" en el segundo, con lo que no podríamos usar estos caracteres juntos.

En el mundo asiático el problema fue aún más complejo, ya que algunos idiomas de la región comprenden miles de caracteres, que obviamente no podían caber en 8 bits. Se intentó utilizar para esto algunos sistemas que usaban dos bytes para codificar un carácter, pero era algo que al final no dejaba de ser otro código especial para un conjunto de idiomas, y además, ni siquiera en 16 bits (65536 posiciones) cabían todos los caracteres posibles.

### Unicode

Unicode es un intento por tener un sólo juego de caracteres que pueda contener cualquier sistema de escritura del mundo, incluidos los pertenecientes a lenguajes extintos o esotéricos. Define 1 114 112 posiciones diferentes.

Unicode no es una codificación como las que hemos visto anteriormente, porque no especifica cómo convertir un carácter a bits, sino que para cada carácter o símbolo especifica un punto de código (code point), que es un identificador numérico. Por ejemplo, el símbolo del euro (€) tiene asociado el punto U+20AC (el prefijo "U+" es simplemente la nomenclatura de Unicode, significa en hex 0x20ac, en decimal 8364). Pero Unicode no determina nada más, no sabemos ni cómo se representa en binario ni cuántos byes ocupa.

Veamos cómo hacer algunas conversiones en Python 3:

```python
>>> ord('€')
8364
>>> hex(ord('€'))
'0x20ac'
>>> chr(8364)
'€'
>>> chr(0x20ac)
'€'
```

Los encargados de hacer esta conversión a bits son los formatos de transformación unicode (UTF). El más habitual, con diferencia, es el UTF-8.

| Unicode       | Codificación directa       | UTF-8                               | Notas          |
| ------------- | -------------------------- | ----------------------------------- | -------------- |
| 000000-00007F | 00000000 0xxxxxxx          | 0xxxxxxx                            | ASCII (1 byte) |
| 000080-0007FF | 00000yyy yyxxxxxx          | 110yyyyy 10xxxxxx                   | 2 bytes        |
| 000800-00FFFF | zzzzyyyy yyxxxxxx          | 1110zzzz 10yyyyyy 10xxxxxx          | 3 bytes        |
| 010000-10FFFF | 000uuuuu zzzzyyyy yyxxxxxx | 11110uuu 10uuzzzz 10yyyyyy 10xxxxxx | 4 bytes        |

Por ejemplo, el símbolo del euro caería dentro del tercer grupo, ya que 000800 < 0x20AC < 00FFFF. La codificación directa en binario (si unicode funcionara igual que ASCII o ISO 8859) sería 10000010101100. Pero vamos a usar UTF-8. Para ello, le añadimos padding al inicio para completar 16 bits: 0010000010101100. Separamos en tres grupos:

- zzzz 0010
- yyyyyy 000010
- xxxxxx 101100

Completamos UTF-8:

1110zzzz 10yyyyyy 10xxxxxx -> 11100010 10000010 10101100

En hexadecimal sería 0xe282ac

El motivo para hacer esto y no una codificación binaria directa...

Una característica muy importante de UTF-8 es que es una codificación de longitud varible. El motivo es que, para estar cómodos con el manejo de más de un millón de posiciones, necesitamos al menos tres bytes (aunque al final se suelen usar cuatro bytes). Pero resultaría absurdo usar cuatro bytes para codificar por ejemplo una letra "a", que además tiene una frecuencia relativa alta en casi cualquier texto en una gran cantidad de idiomas. Esto automáticamente multiplicaría por cuatro el tamaño de un fichero. UTF-32 de hecho hace esto, pero resulta muy poco óptimo. Por eso se prefiere usar UTF-16, que marca el mínimo en 16 bits, y especialmente UTF-8, que para los números y los caracteres del alfabeto latino conserva el mismo tamaño que ASCII, extendiendo a 2, 3 o 4 bytes según sea necesario.

En realidad el concepto de punto de código se puede extender también a ASCII o a los ISO 8859, con la diferencia de que en estos el punto de código se convierte directamente a binario al hacer la codificación, mientras que en unicode existen diferentes reglas y son un poco más complicadas.

## Archivos

Podemos comprobar todo lo que hemos aprendido creando un fichero de texto con un editor, tecleando unos caracteres, guardándolo y abriéndolo de nuevo con un editor hexadecimal.

Aqui hablar de \r\n y de CRLF (Windows) vs LF (Unix)

```bash
$ cat hola.txt
ab
c
€
```

```bash
$ hd -c hola.txt
00000000  61 62 0a 63 0a e2 82 ac  0a                       |ab.c.....|
0000000   a   b  \n   c  \n 342 202 254  \n
0000009
```

## Herramientas

Editor hexadecimal

## Terminología

- Codificar (encode). Sustituir un carácter o una serie de caracteres o símbolos por otros distintos, en base a una relación o unas reglas.
- Decodificar (decode). Recuperar la secuencia original a partir de la codificada, invirtiendo la relación o las reglas de codificación.
- Set de caracteres (character set, charset). El conjunto de caracteres que van a poder ser codificados.
- Página de códigos (code page). La tabla que mapea o relaciona un carácter con su código.
- Punto de código o posición (code point). Valor numérico único que se asocia a un determinado carácter o símbolo.
- Cadena de texto (string). Una serie o secuencia de caracteres.

## Homoglifos

https://www.troyhunt.com/humans-are-bad-at-urls-and-fonts-dont-matter/

https://es.wikipedia.org/wiki/Homoglifo

### Conclusión

Hoy en día en internet se ha llegado a un predominio muy amplio de Unicode/UTF-8, con lo cual se suele asumir que, a la hora de codificar y decodificar, es este estándar el que se utiliza. Sin embargo, hemos aprendido que no tiene sentido crear o leer un texto sin tener en cuenta la codificación que usa, aunque sea implícitamente.

El llamado "texto plano" NO EXISTE.

## Referencias

beneficios base 12 https://www.youtube.com/watch?v=U6xJfP7-HCc

https://realpython.com/python-encodings-guide/

https://www.joelonsoftware.com/2003/10/08/the-absolute-minimum-every-software-developer-absolutely-positively-must-know-about-unicode-and-character-sets-no-excuses/

https://invidious.bmesh.org/watch?v=sgHbC6udIqc&autoplay=0

https://manishearth.github.io/blog/2017/01/15/breaking-our-latin-1-assumptions/

https://manishearth.github.io/blog/2017/01/14/stop-ascribing-meaning-to-unicode-code-points/

https://realpython.com/python-encodings-guide/

https://kunststube.net/encoding/

https://en.wikipedia.org/wiki/Character_encoding#Common_character_encodings
