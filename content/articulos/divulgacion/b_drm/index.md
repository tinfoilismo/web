---
title: DRM y bienes digitales
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/32
draft: true
---

La _compra_ de bienes digitales es un oxímoron; salvo en contados casos muy específicos, lo que se adquiere no es el producto, sino una licencia exclusiva de uso. Esos bienes están sujetos exclusivamente al contrato de licencia de usuario final que la empresa decide y el usuario acepta sin tomar parte en él. Es decir, se acerca más a un alquiler que a una verdadera compra.

Adobe informa a sus usuarios que podrían denunciarles por usar versiones antiguas de Photoshop y otras herramientas de Creative Cloud. https://www.vice.com/en_us/article/a3xk3p/adobe-tells-users-they-can-get-sued-for-using-old-versions-of-photoshop
