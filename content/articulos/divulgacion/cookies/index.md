---
title: Qué son y cómo funcionan las cookies
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/52
etiquetas:
  - eje-privacidad-seguridad
---

![Artículo en inglés titulado "Este bug en tu PC es una cookie inteligente, procedente del Financial Times"](financial-times-cookies.jpg "Artículo aparecido en el Financial Times el 12 de febrero de 1996.")


Una **cookie** es un pequeño bloque de datos, creado por un servidor web, que recibe el navegador cuando accedemos a esa web. El navegador lo almacena en su caché, asociado al dominio del sitio web. En posteriores visitas al mismo sitio web con el mismo dominio, esos datos se vuelven a enviar al servidor automáticamente.

Las cookies son esenciales para ciertas funcionalidades de la web, ya que permiten almacenar información relativa al estado, de modo que convierten a la web en una plataforma con memoria.

## Historia

Antes de la existencia de las cookies, la web no tenía estado, lo cual imposibilitaba cosas tan habituales como hacer login o almacenar preferencias. En 1994 Netscape incorpora las cookies a su navegador web, y muy poco después le seguiría Internet Explorer.

## Para qué sirven

Una cookie no tiene por qué atentar contra la privacidad ni ser invasiva per sé; puede mejorar la experiencia de navegación. Por ejemplo, pensemos en las siguientes situaciones:

- Cuando hacemos una compra online y un item se añade al carrito y permanece ahí hasta que vamos a hacer el pago.
- Accedemos a un sitio web que nos ofrece opciones de configuración o personalización. Por ejemplo, elegir entre usar un tema claro u oscuro.
- Mantener una sesión abierta con una web, de modo que no tengamos que estar continuamente introduciendo nuestras credenciales.

En todos estos ejemplos entran en juego las cookies, que no son más que pequeños archivos de texto que almacenan las credenciales del usuario, o los items que ha añadido a su cesta de la compra, por seguir con los ejemplos anteriores. Estas cookies tienen un ciclo de vida estrictamente adaptado para estos fines, y desaparecen cuando el usuario efectúa la compra (o la descarta) o cuando cierra la sesión.

Sin embargo, en los últimos tiempos, y con la excusa de mejorar la experiencia de usuario, se ha producido un abuso en el uso de las cookies, ya que con ellas se puede llegar a rastrear la actividad del usuario.

## Principales tipos de cookies

Las cookies de autenticación se usan para comprobar si un usuario está logueado, y en su caso, determinar cuál es su identidad. Sin las cookies, los usarios tendrían que autenticarse en cada visita a cada página. La seguridad de una cookie de autenticación depende tanto del servidor que la genera como de la gestión que haga el navegador del usuario, y de si esta cookie viaja cifrada o a través de un canal cifrado. Existen vunerabilidades, que permitirían que una cookie sea leida por un atacante, lo cual le permitiría hacer una suplantación de identidad. Se trata de ataques XSS (cross-site scripting) y CSRF (cross-site request forgery).

Las cookies de seguimento (o de tracking), especialmente las de terceros, se usan habitualmente para recopilar información masiva acerca de los hábitos de navegación de los usuarios.

## Propiedades de una cookie

Si accedemos a las *developer tools* de nuestro navegador (F12) podremos ver las cookies asociadas al dominio actual, así como sus campos.

| Campo | Objetivo | Ejemplo |
| --- | --- | --- |
| Name | Nombre único para identificar la cookie (establecido por el servidor web. Siempre emparejado con el *value* | SessionID |
| Value | Valor asociado al nombre (establecido por el servidor web). Siempre asociado con el *name* | sty1z3kz11mpqxjv648mqwlx4ginpt6c
| Domain | Establece el alcance. Puede ser para un dominio (afectaría a sí mismo y a todos sus subdominios) o para un único subdominio | .tinfoilismo.org
| Path | También establece el alcance. Ruta local para la cookie | / |
| Expires/Max-age | Caducidad de la cookie | 2022-11-11T15:39:04.166Z |
| Size | Tamaño en disco de la cookie, en bytes, típicamente es {Name+Value} | 91 |
| HttpOnly | La cookie no puede ser accedida por el Javascript del lado del cliente | (indicado por un check) |
| Secure | La cookie úncamente puede viajar a través de HTTPS | (indicado por un check) |
| SameSite | Especifica si la cookie se envia a través de peticiones cross-site | none |
| SameParty | Extiende la funcionalidad de *SameSite* a los dominios propios | (indicado por un check) |
| Priority | Define la importancia de una cookie. Determina si debe ser eliminada o conservada | High

## Cokies de terceros

Una limitación clave de las cookies es que, si son generadas por un sitio con un dominio determinado, no pueden ser accedidas por otro sitio con otro dominio. Entonces, ¿cómo es posible efectuar el rastreo a través de todos los sitios web que visita el usuario?

Pongamos el caso de Facebook. Al iniciar sesión, Facebook establece una cookie para recordar sus credenciales, igual que el resto de sitios. Esta sería una cookie normal. A priori, cuando visitamos otro sitio web, por ejemplo, un blog personal, Facebook no puede saberlo. Pero el propietario de este blog puede tener incrustado un botón de "Me gusta" de Facebook. Para mostrar este botón, es necesario obtener y ejecutar código de los servidores de Facebook. Pero este código, además de proveer la imagen, es capaz de generar una cookie a nombre de Facebook. De este modo, Facebook ya sabe que hemos visitado el blog en cuestión.

Evidentemente, esto no solo lo hace Facebook. Facebook puede convencer a mucha gente para poner este botón, ya que muchos blogueros querrán que su publicación tenga "likes" en la plataforma de la red social. Google también lo tiene fácil, suponiendo que el blog en cuestión muestre publicidad para obtener ingresos.

Al contrario que las cookies del propio sitio, las cookies de terceros son creadas por un sitio web cuyo dominio es diferente del que estamos visitando. Este tipo de cookies tienen en general como objetivo efectuar el tracking del usuario.

Supongamos que visitamos un sitio `site1.com` que incrusta banners de `ad.com`. Este último, además de los banners, establece una cookie, pero no para `site1.com`, sino para `ad.com`. Lo que hace es dejar un token en la cookie. Si ahora visitamos `site2.com`, que también muestra banners y puede establecer o leer cookies de `ad.com`. De este modo, `ad.com` puede saber que un mismo usuario ha visitado `site1.com`y `site2.com`, pudiendo así establecer un patrón de comportamiento.

Alegando preocupación por la privacidad de los usuarios, Google ha decidido que su navegador Chrome [dejará de permitir las cookies de terceros](https://maldita.es/malditatecnologia/20210314/google-anuncia-final-cookies-chrome-2022-como-afecta-esto-privacidad-internet/) en el año 2022. El hecho de que sea el navegador predominante hará que las actuales técnicas publicitarias y de seguimiento queden obsoletas. Puede parecer una buena noticia, pero esta jugada de Google está más bien encaminada a controlar y monopolizar el mercado publicitario en la web mediante algo llamado [FLoC](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea).

## Supercookies

También conocidas como *cookies zombie* o *permacookies*. El uso de supercookies para tracking se detalla en este paper[^paper-supercookies].

supercookies de botones "me gusta" de facebook
supercookies por ISPs

## Regulación

Si bien hay una regulación de cookies y se pide a los sitios web que expliciten qué información obtienen de sus visitantes, no se explica nunca con un lenguaje suficientemente accesible, y son habituales los *dark patterns* que incitan al usuario a aceptar todo tipo de rastreadores. Por ejemplo, teniendo que desmarcar decenas de casillas, una por una, en cada visita a una web, ofreciendo botones pequeños, con colores apagados y poco accesibles para no aceptar.

## Referencias

Hussein Nasser
https://www.youtube.com/watch?v=m4vatwFryI8
https://www.youtube.com/watch?v=sovAIX4doOE
https://www.youtube.com/watch?v=aUF2QCEudPo
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie/SameSite

Computerphile
https://www.youtube.com/watch?v=T1QEs3mdJoc
https://www.youtube.com/watch?v=LHSSY8QNvew
https://www.youtube.com/watch?v=_d0G6FZ_kR4
