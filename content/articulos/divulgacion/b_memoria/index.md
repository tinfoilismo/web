---
title: Memoria y dependencia tecnológica
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/44
draft: true
---

# Esquema
La memoria:
- como instrumento falible.
- como elemento que requiere entrenamiento y mantenimiento.
- insuficiente para lo que el ser humano necesita. Las prótesis externas.
- y la sobreinformacion.
- en el aprendizaje.
- y la atención.
- El ser humano sin memoria propia.
- La sociedad sin memoria propia.
