---
title: Ley Orgánica de Protección de Datos Personales y garantía de los derechos digitales
etiquetas:
  - eje-privacidad-seguridad
aliases:
  - /docs/regulación/lopd-gdd
  - /wiki/lopd-gdd/
# enlaces rotos a "burbuja de filtros" en lineas 29, 40 y 46
---

La **Ley  Orgánica  3/2018,  de  5  de  diciembre,  de  Protección  de  Datos Personales y garantía de los derechos digitales**, abreviada como **LOPD-GDD**, es la ley española que adapta el derecho interno español al [RGPD europeo]({{< relref rgpd >}}) y deroga a la anterior Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal. Entró en vigor el 7 de diciembre de 2018.

El documento consolidado en el Boletín Oficial del Estado (BOE-A-2018-16673) está disponible para su descarga en PDF y ePUB y para consulta online en https://www.boe.es/eli/es/lo/2018/12/05/3/con

## Controversias

### Recopilación masiva de datos de índole política para fines propagandísticos opacos

La disposición final tercera de la Ley introduce una exención a los partidos políticos, por considerarlo «amparado por el interés público», que permite recopilar datos relativos a las opiniones políticas de las personas en el marco de las actividades electorales de los partidos. Es decir, la [confección y explotación electoral de perfiles ideológicos y personales]({{< relref capitalismo-de-vigilancia >}}) de cualquier ciudadano, lo que algunos juristas denominan como «la legalización del [Cambridge Analytica](burbuja-de-filtros#2018) español».[^1] [^2] [^3] Así mismo, se habilita a los partidos políticos a utilizar datos personales obtenidos en páginas web y otras fuentes de acceso público para fines como [enviar propaganda electoral por medios electrónicos](burbuja-de-filtros) y sin consentimiento previo.[^1] [^2] [^3] [^4]

Este es el texto de la LOPD-GDD que modifica la de la Ley Orgánica de Régimen Electoral General (LOREG) introduciendo el artículo 58 bis:

> **Artículo 58 bis.** Utilización de medios tecnológicos y datos personales en las actividades electorales.
>
> 1. La recopilación de datos personales relativos a las opiniones políticas de las personas que lleven a cabo los partidos políticos en el marco de sus actividades electorales se encontrará amparada en el interés público únicamente cuando se ofrezcan garantías adecuadas.
>
> 2. Los partidos políticos, coaliciones y agrupaciones electorales podrán utilizar datos personales obtenidos en páginas web y otras fuentes de acceso público para la realización de actividades políticas durante el periodo electoral.
>
> 3. El envío de propaganda electoral por medios electrónicos o sistemas de mensajería y la contratación de propaganda electoral en redes sociales o medios equivalentes no tendrán la consideración de actividad o comunicación comercial.
>
> 4. Las actividades divulgativas anteriormente referidas identificarán de modo destacado su naturaleza electoral.
>
> 5. Se facilitará al destinatario un modo sencillo y gratuito de ejercicio del derecho de oposición.

A pesar de que el último punto obliga a facilitar un método de control para el ciudadano, el tener que **oponerse** (_opt-out_) al tratamiento de esos datos va en la dirección contraria a las prácticas que el [RGPD]({{< relref rgpd >}}) trata de implantar generalmente, que es el **consentimiento previo** (_opt-in_). También preocupa a los expertos el uso que los partidos políticos pueden hacer de la propaganda personalizada, puesto que la naturaleza opaca de la comunicación personal impide que haya ningún tipo de control sobre las prácticas de juego sucio que puedan existir, como el uso de la desinformación mediante noticias falsas y la manipulación mediante el miedo.[^1]

📝 La Plataforma en Defensa de la Libertad de Información (PDLI), la Asociación de Internautas y expertos juristas crearon un **formulario para ejercer tu derecho de oposición** frente a los partidos políticos.[^5] Puedes encontrarlo al final de esta página: http://libertadinformacion.cc/la-pdli-asociacion-internautas-y-juristas-especializados-lanzan-un-formulario-para-impedir-que-los-partidos-puedan-crear-bases-de-datos-con-las-opiniones-politicas-de-los-ciudadanos/

⚖️ 5 de marzo de 2019. El Defensor del Pueblo recurrirá ante el Tribunal Constitucional la nueva LOPD por 'espionaje' ideológico. https://www.elconfidencial.com/tecnologia/2019-03-05/defensor-pueblo-lopd-tribunal-constitucional_1863046/ &middot; http://libertadinformacion.cc/el-recurso-del-defensor-del-pueblo-contra-la-nueva-ley-de-proteccion-de-datos-confirma-el-atropello-que-representa-esta-norma-para-los-derechos-fundamentales/

📝 7 de marzo de 2019. La Agencia Española de Protección de Datos publica una circular por la que limita el acceso a los datos personales por parte de los partidos políticos. https://www.boe.es/diario_boe/txt.php?id=BOE-A-2019-3423 &middot; https://elpais.com/sociedad/2019/03/11/actualidad/1552310428_533487.html

⚖️ 12 de marzo de 2019. El Tribunal Constitucional acepta a trámite el recurso del Defensor del Pueblo. https://www.tribunalconstitucional.es/NotasDePrensaDocumentos/NP_2019_029/NOTA%20INFORMATIVA%20N%C2%BA%2029-2019.pdf &middot; http://libertadinformacion.cc/un-paso-mas-para-la-inconstitucionalidad-de-las-tecnicas-de-perfilado-ideologico-para-propaganda-electoral/

⚖️ 22 de mayo de 2019. El Tribunal Constitucional anula por unanimidad el apartado 1 del artículo 58 bis de la Ley Orgánica de Régimen Electoral General (LOREG) que habilitaba a los partidos a recopilar datos sobre las opiniones políticas de los ciudadanos para crear perfiles ideológicos. https://www.tribunalconstitucional.es/NotasDePrensaDocumentos/NP_2019_074/NOTA%20INFORMATIVA%20N%C2%BA%2074-2019.pdf &middot; http://libertadinformacion.cc/victoria-de-la-sociedad-civil-el-constitucional-declara-inconstitucional-y-nulo-el-perfilado-ideologico-para-propaganda-electoral/ &middot; https://www.eldiario.es/tecnologia/perfilado-ideologico-unanimidad-Congreso-Constitucional_0_901860559.html &middot; https://elpais.com/politica/2019/05/22/actualidad/1558530147_953979.html


## Referencias

[^1]: 19 de noviembre de 2018. _Los partidos políticos impulsan sin debate una ley que les permitirá enviar al móvil propaganda electoral sin consentimiento_ (eldiario.es). https://www.eldiario.es/tecnologia/partidos-enviarte-propaganda-politica-consentimiento_0_837466634.html
[^2]: 7 de diciembre de 2018. _Llega la ley que «espía» tu ideología: los partidos podrán recopilar tus datos sin consentimiento_ (ABC). https://www.abc.es/tecnologia/redes/abci-llega-ley-espia-ideologia-partidos-podran-recopilar-datos-sin-consentimiento-201811211243_noticia.html
[^3]: 16 de diciembre de 2018. _Espionaje en Internet: Los partidos ya pueden rastrear tus opiniones políticas_ (COPE). https://www.cope.es/actualidad/noticias/gag-20181216_310109
[^4]: 22 de noviembre de 2018. _Todo lo que podrán hacer los partidos con tus datos gracias al ‘Gran Hermano político’_ (El independiente). https://www.elindependiente.com/politica/2018/11/22/partidos-politicos-propaganda-personalizada-gran-hermano-ley-organica-proteccion-datos/
[^5]: 6 de diciembre de 2018. _Protégete de la LOPD: cómo evitar que los partidos políticos recopilen tus datos_. https://www.elconfidencial.com/tecnologia/2018-12-06/protegerse-lopd-bases-de-datos-partidos-politicos_1690150/
