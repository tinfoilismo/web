---
title: Navegación privada/incógnito y navegador pantalla
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/39
draft: true
---

Background: economía de vigilancia en la Web, rastreadores por todas partes, móviles personales donde hacemos búsquedas banales que quedan registradas. Evidencia de que nuestros navegadores tienen un montón de información sobre nosotros que comparten con cada página que visitamos (cookies, etc).


## El modo incógnito o la navegación privada

https://maldita.es/malditatecnologia/2020/06/22/modo-incognito-navegador-no-guarda-historial-navegacion-no-anonimo-privacidad/

No es anónimo: enlazar al artículo de tor.md


## Navegador pantalla

Un **navegador pantalla**, o _proxy browser_, es un navegador web olvidadizo: te protege con navegación privada (no recuerda tu historial) y bloqueo de balizas web. La idea es utilizarlo como tu navegador de cabecera para buscar contenido, porque pasarás por muchas páginas que jamás volverás a visitar. Cuando encuentres lo que buscas, entonces abre tu navegador preferido, ese que tiene toda tu información.

Si somos pseudoanónimos por defecto (ventana privada/modo incógnito), realmente le hacemos mucho daño a la «economía de la vigilancia» mientras, a la vez, protegemos nuestra privacidad. ¿Por qué Facebook debería saber cada paso que doy en internet incluso cuando no tengo perfil? (Es más sencillo utilizar esto que una extensión como Facebook Container para cada empresa, p.e.)


Uso sin JavaScript: ¿Por qué debería tener JavaScript activado si la mayoría de páginas que consulto son solo texto (artículos, páginas de información general, etc)? JavaScript es interacción, no necesitas interacción mientras buscas, solo cuando te estableces en una página, que es cuando cambiarías a tu navegador habitual. Os sorprenderías, p.e., de la cantidad de cosas absurdas que carga un periódico en línea, ninguna de las cuales afecta al texto principal pero sí repercute en cómo #Facebook o #Google os muestran línea de tiempo y resultado de búsquedas. Burbujas de información, se llaman.


@soyguijarro: un punto a considerar de los «proxy browsers»: cuando finalmente cambias a tu navegador habitual, el contenido se vuelve a descargar y por tanto cuentan doble los megas (una por Focus y otra por tu navegador). Aunque con JavaScript y las tipografías desactivadas, las páginas son más livianas y puede ocurrir al contrario: solo cargo completo (si resulta necesario) el resultado final de una búsqueda.


### Ejemplos de navegadores pantalla (móviles)

Firefox Focus/Klar: de código abierto y libre; es decir, completamente transparente, por lo que puedes confiar. Solución estupenda para los que decidimos navegar sin JavaScript porque te permite desactivarlo. Klar es la versión alemana (en F-Droid).

DuckDuckGo Privacy Browser?: https://duckduckgo.com/app (está en F-Droid y descarga de APK)


No se conocen navegadores pantalla para sistemas de escritorio
