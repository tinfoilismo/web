---
title: Qubes OS
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/42
draft: true
---

Esquema básico del artículo:

- Introducción: que es QubesOs, quien lo desarrollo. Motivación de uso (cita Snowden)
    - QubesOs es un sistema operativo que funciona con hipervisor Xen (tipo1) sobre metal,
    - Desarrollado por Johanna Rukosvska, hacker y experta en ciberseguridad.
    - cita snowden (https://twitter.com/snowden/status/781493632293605376?lang=es):
    *If you're serious about security, @QubesOS is the best OS available today. It's what I use, and free. Nobody does VM isolation better.*
- Arquitectura: hipervisor tipo. 1 Xen. Bare metal: diferencia con la virtualización de tipo 2 y ventajas. Servicios: escritorio vs. gestión de recursos.
    - Hipervisor tipo 1 significa que no carga un sistema operativo sobre el que luego levantar un gestor de MV, si no q el mismo es el gestor, por eso está un nivel por encima. Todos sus recursos con virtualizados, el escritorio, la ed, el sistema de archivos. Para trabajar con el hay dierentes entornos.

- Diferentes entornos e trabajo. Desde untrusted hasta Vault.
    - Entorno de trabajo es una maquina virtual que se levanta aislada del resto de maquinas y dle mismo host, tiene sus recursos, gestionados por el el hipervisor. Este aislamiento es lo que se conoce como Cubos y es lo que le da su nombre.  Hay diferentes entornos de trabajo, y son clasificados por el tipo de seguridad que tienen. El verda o trusted es un entorno de trabajo que dificilmente puede verse comprometido, puede ser un navegador de acceso al banco,  y el entornos untrusted de color rojo, es el entonrno menos seguro.
    Ninguno de ellos se comunica con el host, tan solo conviven y en caso de que alguno se vea comprometido, ninguno de sus hermanos será vulnerable.

- Torificacion de la salida / whonix.
    - Se puede tomar la opcion de salir con el trafico torificado por defecto, esto levanta una máquna virtual Wonix y ella se encarga de funcionar como proxi para el resto --> confirma esto!!!

- Formas de instalación. Live USB installation.
    - Se recomienda visitar la HLC de QubesOs
    - Descargar el instalador de la palgina oficial (vinculo).
    - Montar como live USB este instalador.
    - Si se realiza en un disco duro, instalar QubesOs el último para que lidere del GRUB
    - Se puede instalar en un live USB para mayor seguridad y privacidad.
    -
- Importancia del cambio de paradigma para trabajar con QubesOs.
    - Utilizar sandvox para sesiones de las que no es necesario guardar información.
    - No copiar archivos de una máquna de menor seguridad a una de mayor seguuridad,
    - No compartir archivos gratuitamente.
    - No utilizar maquinas seguras para actividades inseguras.
    - Tener diferentes maquinas para diferentes actividades.
    - tener mucho cuidado con que se instala en una plantilla que es lo q usa QubesOs para crear las maquinas virtuales.
