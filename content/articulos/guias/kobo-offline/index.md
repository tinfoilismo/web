---
title: "Utilizar un lector-e Kobo sin conectarlo a internet"
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/60
summary: Indicaciones para usar y actualizar un lector de libros electrónicos Kobo sin conexión a internet y sin crear una cuenta de Kobo.
etiquetas:
  - eje-descentralizacion
  - eje-privacidad-seguridad
---

Al contrario que los libros en papel, los libros electrónicos (_e-books_, en inglés, o «libros-e») están sujetos a las imposiciones de los fabricantes de lectores de libros electrónicos (_e-readers_, en inglés, o «lectores-e») y de las distribuidoras de los libros-e. Dos de las limitaciones más conocidas son el bloqueo del estándar ePub para publicaciones electrónicas por parte de Amazon en sus lectores-e Kindle y el uso de sistemas DRM abusivos por las editoriales y distribuidoras, que impiden u obstaculizan usos legítimos de los libros-e como, por ejemplo, trasladar una obra adquirida a un nuevo dispositivo de distinta marca.

Los lectores-e de la marca **Kobo** pueden mostrar adecuadamente los libros sin DRM en el formato estándar ePub, pero nos encontramos con la imposibilidad de usar por primera vez el lector-e sin conectarlo a internet o con la obligación de crear una cuenta en la tienda de libros-e del fabricante y distribuidor.

En esta guía se describe un método para utilizar cualquier lector-e de la marca Kobo sin estas limitaciones, además de indicar cómo actualizar el sistema operativo del dispositivo y descargar los diccionarios oficiales sin conexión a internet.

## Evitar el registro inicial del dispositivo

Cuando enciendes un lector-e Kobo por primera vez, la configuración inicial **bloquea** cualquier interacción hasta que conectas el dispositivo a un punto WiFi con acceso a internet, o alternativamente, hasta que ejecutas un programa de configuración en un ordenador con sistema operativo Windows o Mac que esté conectado a internet. Esta configuración inicial hace dos cosas: primero, te obliga a crear una cuenta en `kobobooks.com`, la tienda de libros-e de Kobo, y actualiza el _software_ de tu dispositivo Kobo.

Algunos usuarios preferirían no conectar sus dispositivos a internet o crear cuentas _online_ por motivos de **privacidad**, mientras que otros no estarían interesados en adquirir sus libros-e en la tienda _online_ de Kobo. Además, el programa de configuración alternativo no está disponible para otros sistemas operativos como **GNU/Linux**, lo que deja a los usuarios de estos últimos sin opción a no conectar su dispositivo a internet.

Afortunadamente, hay una sencilla manera de saltarse la restricción anteriormente mencionada: el dispositivo Kobo saltará esta configuración inicial si detecta que ya hay una cuenta de usuario registrada en el sistema. Podemos añadir cuentas falsas editando la base de datos interna del dispositivo.[^mobileread-kobo-standalone]

Hay dos maneras de conseguir esto: una para usuarios promedio y otra para usuarios avanzados.

### Opción más sencilla para usuarios promedio

1. Conecta tu dispositivo Kobo al ordenador usando un cable USB; su contenido se mostrará como el de cualquier otra memoria USB.
2. Instala en tu ordenador el programa [DB Browser for SQLite](https://sqlitebrowser.github.io/sqlitebrowser/) («explorador de bases de datos SQLite») y abre con él el archivo `KoboReader.sqlite` que encontrarás dentro de la carpeta `.kobo` de tu dispositivo.
3. Da clic en la pestaña `Hoja de datos` y selecciona la tabla `user` en el desplegable de opciones.
4. Da clic en el botón `Insertar un nuevo registro en la tabla actual` para añadir una nueva cuenta de usuario. Se añadirá una nueva fila a la tabla. Si das doble clic sobre las celdas, podrás editar el valor de cada una de ellas.
5. Introduce un nombre falso (por ejemplo, «Maniquí») en la celda `UserDisplayName`.
6. Introduce una dirección de correo-e falsa (por ejemplo, «filemon@agentes.tia») en la celda `UserEmail`. No necesita ajustarse al formato estándar de direcciones de correo-e.
7. Guarda los cambios pulsando en `Archivo` → `Guardar cambios` en la barra de menú superior.
8. Desconecta con seguridad el dispositivo Kobo de tu ordenador, como harías con cualquier otra memoria USB.

### Opción más directa para usuarios avanzados

<details>
  <summary>Asumiendo que ya tengas el cliente de terminal de SQLite3…</summary>

1. Abre la base de datos desde la raíz del almacenamiento del lector ejecutando `$ sqlite3 .kobo/KoboReader.sqlite` en una terminal (Linux, Mac) o con un comando similar en el símbolo de sistema (Windows).
2. Añade un nuevo registro a la tabla `user` ejecutando la siguiente consulta:
    ```
    INSERT INTO 'user' (UserID, UserKey, UserDisplayName, UserEmail)
    VALUES (3, '', 'Maniquí', 'filemon@agentes.tia');
    ```
    Puedes cambiar los valores: `UserID` es un `NOT NULL PRIMARY KEY` y `UserEmail` no tiene que ajustarse al estándar de direcciones de correo-e.
3. Sal ejecutando `.exit`.

</details>

## Actualizar el sistema operativo del lector-e sin conexión a internet

Sin conexión a internet, el sistema operativo de tu lector-e no se actualizará solo. Las actualizaciones incluyen mejoras, y ocasionalmente, también nuevas funcionalidades para tu dispositivo, por lo que es interesante actualizarlo de cuando en cuando.

Con la ayuda de un ordenador para descargar el paquete de **actualización oficial**, el lector-e se puede actualizar sin conectarlo a internet a través de un proceso manual muy sencillo.

### Opción más sencilla para usuarios promedio

1. En un ordenador, descarga el archivo ZIP que corresponda a tu dispositivo de la lista que encontrarás a continuación; ese archivo contiene la actualización oficial de software y proviene directamente de los **servidores de Kobo**.
2. Conecta tu dispositivo Kobo al ordenador usando un cable USB; su contenido se mostrará como el de cualquier otra memoria USB.
3. Extrae los contenidos del archivo ZIP en la carpeta `.kobo` que encontrarás entre el contenido de tu dispositivo.
4. Finalmente, desconecta con seguridad el dispositivo Kobo de tu ordenador, como harías con cualquier otra memoria USB; el lector-e se reiniciará y empezará el proceso de actualización automáticamente.

### Opción más personalizable para usuarios avanzados

<details>
  <summary>Para personalizar la petición HTTP…</summary>

El _endpoint_ de los servidores de Kobo para comprobar actualizaciones no requiere de ningún tipo de autenticación. Es el siguiente:

```
GET https://api.kobobooks.com/1.0/UpgradeCheck/Device/<identificador de modelo>/<afiliado comercial>/<versión de firmware actual>/<número de serie>
```

La respuesta a la petición es un JSON con esta forma:

```
{
  "UpgradeType": 3,
  "UpgradeURL": "https://kbdownload1-a.akamaihd.net/firmwares/<hardware>/Month20xx/kobo-update-x.xx.xxxxxx.zip",
  "ReleaseNoteURL": "https://api.kobobooks.com/1.0/ReleaseNotes/xxx",
  "Data": null
}
```

Los datos entre los símbolos `<>` se pueden extraer del fichero `.kobo/version` que puedes encontrar en el almacenamiento de tu dispositivo. Contiene datos separados por comas con la siguiente forma:

```
Nxxxxxxxxxxxx,3.0.35+,4.31.19086,3.0.35+,3.0.35+,00000000-0000-0000-0000-000000000yyy
```

En orden, los datos son: número de serie (las `x` son dígitos), desconocido, versión de _firmware_ actual, desconocido, desconocido, identificador de modelo (las `y` son dígitos).

Por ejemplo, si fuera a actualizar un modelo Kobo Aura Edition 2 (revisión de _hardware_ v1), usaría el identificador de modelo `00000000-0000-0000-0000-000000000375`. Como afiliado comercial se puede usar `kobo`, la versión de _firmware_ actual puede ser `0.0` y para el número de serie es suficiente con `N`. Si utilizase `curl` como cliente HTTP para hacer la petición `GET` y `jq` para extraer la dirección con el paquete de actualización, el comando sería el siguiente:

```sh
curl https://api.kobobooks.com/1.0/UpgradeCheck/Device/00000000-0000-0000-0000-000000000375/kobo/0.0/N | jq ".UpgradeURL"
```

</details>

### Descargas de paquetes de actualización

(Si tu modelo es un Edition 2, [lee más abajo antes de descargar](#identificar-la-versión-de-los-modelos-edition-2))

{{< kobo-firmware-downloads >}}

#### Identificar la versión de los modelos Edition 2

Los modelos Kobo Aura H2O Edition 2 y Kobo Aura Edition 2 se han comercializado con dos revisiones de _hardware_. Para saber a cuál corresponde tu lector-e, sigue estos sencillos pasos:[^mobileread-edition-2-models]

1. En el lector-e, toca el icono del menú `≡ Más` en la esquina inferior derecha y navega hasta `Configuración` → `Información del dispositivo`.
2. Localiza, en el número de serie, el séptimo caracter (o el sexto dígito):
	1. si es un cero (`0`), es de los primeros, la revisión v1.
	2. si es un dos (`2`), es la actualización, la revisión v2.

## Instalar diccionarios sin conexión a internet

1. Descarga los archivos ZIP correspondientes a los idiomas que quieras consultar de la lista que encontrarás a continuación.
2. Conecta tu dispositivo Kobo al ordenador usando un cable USB; su contenido se mostrará como el de cualquier otra memoria USB.
3. Copia los archivos `dicthtml.zip` a la carpeta `dict` dentro de la carpeta `.kobo` del dispositivo. **No extraigas el contenido de los archivos**.
4. Desconecta con seguridad el dispositivo Kobo de tu ordenador, como harías con cualquier otra memoria USB
5. En el lector-e, toca el icono del menú `≡ Más` en la esquina inferior derecha y navega hasta `Configuración` → `Idioma y diccionarios`.
6. Allí podrás leer: «X diccionarios instalados». Toca el botón `Editar` a la derecha.
7. Habilita cada uno de los diccionarios que has descargado pulsando el botón `+` que hay a su lado.

### Descargas de diccionarios

{{< kobo-dictionary-downloads >}}

## Fuentes y recursos similares

_Kobo Offline_, página web del colaborador Roboe con instrucciones y descargas para utilizar dipositivos Kobo sin conectar a internet (en inglés). https://kobo-offline.virgulilla.com/

_KoboStuff Firmware Downloads_, página web del programador canadiense Patrick Gaskin donde descargar las actualizaciones de _software_ de los distintos modelos de Kobo (en inglés). https://pgaskin.net/KoboStuff/kobofirmware.html

Hilo del usuario morg en el foro MobileRead con enlaces directos de descarga de diccionarios e instrucciones para instalarlos (en inglés). https://www.mobileread.com/forums/showthread.php?t=196931

## Referencias

[^mobileread-kobo-standalone]: Respuestas de los usuarios rashkae y Koboyashi en el foro MobileRead con instrucciones para saltar el registro de dispositivo inicial (en inglés). https://www.mobileread.com/forums/showthread.php?p=1998068#post1998068

[^mobileread-edition-2-models]: Respuesta del usuario davidfor en el foro MobileRead con información de cómo identificar las distintas versiones de los modelos Kobo Aura H2O Edition 2 y Kobo Aura Edition 2 (en inglés). https://www.mobileread.com/forums/showpost.php?p=3678644&postcount=223
