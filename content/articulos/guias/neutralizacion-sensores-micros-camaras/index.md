---
title: Neutralización de sensores, micrófonos y cámaras
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/23
etiquetas:
  - eje-privacidad-seguridad
aliases:
  - /docs/guías/kill-switches
  - /wiki/kill-switches/
---

Los dispositivos electrónicos actuales cuentan con una gran variedad de sensores de entrada, como lectores de huella dactilar, cámaras y micrófonos. Salvo en contadas excepciones, el diseño y colocación de estos sensores hace que el usuario no pueda decidir si aportar esa información de caracter personal o pueda decidir en qué momentos concretos hacerlo, además de suponer siempre que el sistema no puede ser comprometido.

Este artículo recoge una colección de maneras, tanto caseras como comerciales, de limitar el alcance de estos sensores y retomar el control. Se los denomina _kill switches_ (interruptores de apagado) o prótesis de seguridad. Dispositivos centrados en la privacidad como [la línea Librem][purism-librem] de portátiles y el teléfono Librem 5 de Purism y el [PinePhone][pine64-pinephone] de Pine64 traen interruptores físicos.


## Cámaras

### Cámaras frontales

Las cámaras ópticas y las cámaras de infrarrojos o profundidad que apuntan directamente a los rostros de los usuarios pueden ser neutralizadas sencillamente, aunque se puede complicar cuanto más fino sea el marco o si están integradas en la pantalla.

La forma **casera** de hacerlo es tapando la cámara con pegatinas o notas adhesivas (_post-it_). La desventaja es que la inutiliza por completo, dejan marcas de pegamento o se deterioran con facilidad. Utilizar ojos adhesivos compone una icónica metáfora.

Por otro lado, tenemos pegatinas **comerciales** que cuentan con una tapa deslizante para que el usuario pueda decidir cuándo destapar la cámara. Son preferibles las de algún tipo de metal frente a las de plástico, porque la tapa de estas últimas puede desacoplarse con más facilidad o tomar holgura.

Una precaución a tomar en los teléfonos con pantalla táctil es no tapar o interferir con el sensor de proximidad, que se encarga de apagar correctamente la pantalla cuando nos acercamos el teléfono a la oreja. Suele estar colocado a la izquierda, pero se puede dar el caso de que esté centrado.

### Cámaras traseras

En el caso ideal, se pueden utilizar los mismo métodos que para las cámaras frontales. Sin embargo, en numerosos modelos de teléfono, la cámara trasera sobresale de la carcasa, lo que dificulta instalar tapas deslizantes por tamaño o porque tapan el flash.


## Micrófonos

Los micrófonos son más difíciles de neutralizar porque taparlos no es suficiente. Además, los dispositivos hoy en día traen al menos dos micrófonos, uno principal y otro para la cancelación de ruido.

También es conveniente recordar que [el giroscopio puede utilizarse como micrófono](https://crypto.stanford.edu/gyrophone/).

### Solución protésica

Una posible solución es conectar un falso micrófono externo ([_dud audio jack dongle_](https://www.reddit.com/r/privacy/comments/8c4rq6/does_a_dud_audio_jack_dongle_actually_stop_your/)). Se trata de un aparato que [se conecta al jack de audio](https://www.reddit.com/r/privacy/comments/86ota4/my_mini_inventionhardware_hack_deactivate_the/) para que el software piense que tiene un micrófono conectado sin tenerlo realmente.

Puedes tratar de crear uno **casero** [cortando el conector de un antiguo micrófono](https://www.wired.com/2014/03/webcams-mics/). Es posible que sea necesario soldar la resistencia adecuada para que el dispositivo lo reconozca.

Por otro lado, el producto **comercial** se conoce como _mic blocker_. Algunos como [Mic-Lock](https://mic-lock.com/) permiten conectar unos cascos de audio para no perder esa posibilidad.

### Solución quirúrgica

Otra opción **casera** es [amputar los micrófonos del dispositivo](https://web.archive.org/web/20160402005909/https://people.torproject.org/~ioerror/skunkworks/moto_e/). Cuando sea necesario, se puede conectar un micrófono por cable o por Bluetooth.

Desmontar el dispositivo puede tener mayor o menor dificultad. En [iFixit][ifixit] se recogen unas excelentes guías libres que te pueden ayudar. En el caso especial del [Fairphone 2][fairphone-2] y el [Fairphone 3][fairphone-3], el micrófono principal y el de cancelación de ruido forman parte del módulo inferior (carga) y del módulo superior (cámara frontal), respectivamente. Es posible conseguir las piezas de repuesto y extirpar los micrófonos, o [conseguir módulos rotos en la sección #Market/Giveaway del foro comunitario](https://forum.fairphone.com/c/market/giveaway).

### Solución acaparadora (experimental)

En teléfonos Android, existe una opción experimental, llamada PilferShush Jammer. El micrófono solo se puede ceder a una aplicación a la vez, así que la técnica consiste en que una aplicación acapare el uso del micrófono de manera que ninguna otra pueda hacerlo. Sin embargo, no bloqueará aplicaciones del sistema (como el asistente de voz de Google). Puedes encontrar [más información en su página de GitHub](https://github.com/kaputnikGo/PilferShushJammer) y [descargarla en F-Droid](https://f-droid.org/en/packages/cityfreqs.com.pilfershushjammer/).

### Asistentes de voz

En el terreno de los **asistentes de voz**, tenemos un par de opciones.

[Project Alias](http://bjoernkarmann.dk/project_alias) es un aparato experimental que se coloca encima de un asistente de voz y parasita su funcionamiento, emitiendo ruido directamente a los micrófonos para bloquear el reconocimiento de voz. Además, sirve para cambiar el nombre con el que se activa el asistente. Puedes [fabricártelo tú mismo](https://www.instructables.com/id/Project-Alias/).

[Sebastian](https://www.xataka.com/accesorios/asistente-que-controla-al-asistente-este-interruptor-voz-genial-invento-para-decidir-cuando-queremos-ser-escuchados) es un asistente de voz sin conexión a internet que controla a otro asistente comercial gestionando si el segundo recibe o no electricidad, de manera que puedes apagar o encender el asistente de voz comercial. También te recuerda si el asistente está encendido. Por el momento, no existen instrucciones detalladas de cómo fabricarlo ni se comercializa.

### Hazte tu propio interruptor

Si tienes el conocimiento necesario, no serías el primero en [soldar un interruptor físico](http://stahlke.org/dan/phonemute/).


## Lector de huellas

La manía o la arrogancia y total falta de respeto a la privacidad ha llevado a muchos fabricantes a diseñar dispositivos con lectores de huellas. Una _contraseña_ que te venden como la panacea de la seguridad, pero que no se puede cambiar y que es _tan infalible_ que siempre se acompaña de una contraseña tradicional. Tu biometría a cambio de nada.

### En la trasera del dispositivo

Este lector es totalmente opcional pero está colocado en un lugar peligroso. La solución **casera** es taparlo con una pegatina o cinta adhesiva.

Otra posible solución **casera** es amputarlo. Ayúdate de las guías de [iFixit][ifixit]. El [Fairphone 3][fairphone-3] vuelve a ser el más sencillo para este propósito.

### Integrado en un botón pulsado o capacitivo frontal

Un botón no se puede amputar tan sencillamente. Aunque si tienes control sobre un sistema Android, podrías activar los botones en pantalla, y si tienes un iPhone, puedes utilizar un botón de repuesto no oficial, que no traen lectores de huellas porque [Apple no permite que piezas no originales sean compatibles con Touch ID](https://es.ifixit.com/Guía/Reemplazo+del+Bot%C3%B3n+de+Inicio+del+iPhone+5s/24983).

En cualquier caso, una solución **casera** y efectiva es utilizar una pegatina, cinta aislante o un protector de pantalla cortado al tamaño adecuado. Si la pegatina es transparente, no habrá ninguna señal aparente de la neutralización.

Con los botones pulsados no hay ningún problema. Si el botón es capacitivo (es decir, detecta el toque de tu dedo y vibra), es posible que no valga una pegatina demasiado gruesa. Trata de buscar un material más fino, conductivo o poroso.

### Integrado en pantalla

Por investigar. No compres ese dispositivo si puedes evitarlo.


[purism-librem]: https://en.wikipedia.org/wiki/Librem
[pine64-pinephone]: https://en.wikipedia.org/wiki/Pine_Microsystems#Smartphone

[ifixit]: https://ifixit.com/
[fairphone-2]: https://es.wikipedia.org/wiki/Fairphone_2
[fairphone-3]: https://en.wikipedia.org/wiki/Fairphone_3
