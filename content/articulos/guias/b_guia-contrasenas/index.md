---
title: Cómo elegir contraseñas seguras
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/4
draft: true
---

_Traducción de un artículo en inglés del blog de la Fundación Wikimedia.[^artículo-original] El artículo original está licenciado bajo la [licencia de cultura libre Creative Commons Attribution-ShareAlike 3.0 Unported license](https://creativecommons.org/licenses/by-sa/3.0/), por lo que este también._


La mayoría de las brechas de seguridad en Internet están relacionadas con contraseñas robadas o débiles. Queremos construir sobre la cultura de seguridad dentro del movimiento Wikimedia para proteger las cuentas de ser comprometidas.

Puede que recuerdes un tiempo antes de los estándares de contraseñas, cuando se usaban contraseñas como "password". Como innumerables noticias nos han mostrado desde entonces, esas contraseñas no eran ideales, pero la solución recomendada de crear contraseñas complejas para cada sitio web ha creado problemas propios.[1]

Tener una de tus cuentas online hackeadas puede ser una experiencia perturbadora y molesta. Por eso, el equipo de seguridad de la Fundación Wikimedia quiere facilitar la prevención actualizando nuestras políticas de contraseñas (más información en la parte inferior) y ha reunido seis reglas para seleccionar una buena contraseña[2]. Recomendamos encarecidamente a todos los usuarios actuales de Wikimedia que revisen la política actualizada y las contraseñas actuales para asegurarse de que su cuenta sigue siendo segura.
Regla #1: Favorecer la longitud sobre la complejidad

Al crear una contraseña, elige algo que sea fácil de recordar pero que tenga muchos caracteres y esté compuesto por varias palabras. Me gusta usar una colección de pensamientos y cosas para crear una declaración o frase. Esta frase podría ser un disparate o algo real.

Foto de John Bennett, CC BY-SA 4.0.

Por ejemplo, aquí hay una foto de un perro. Si creara una contraseña basada en esta imagen, sería "¡Ese perro está parado en las violetas y necesita un afeitado!" Es una gran contraseña por estas tres razones: es larga, difícil de adivinar o de descifrar, y fácil de recordar (porque es verdadera).

Una contraseña más compleja con menos caracteres, como D0gg@sRul3!, es difícil de descifrar pero mucho más difícil de crear y recordar. Como es difícil de recordar, también es más probable que se recicle para usarla en otros lugares, lo cual es una mala idea y algo que cubriremos en la regla #5.
Regla #2: D0nt M@k3 1t h/\rd3r t#aN 1T hA5 t() %e! (¡No lo hagas más difícil de lo que tiene que ser!)

La complejidad es el enemigo de la seguridad. Desde el punto de vista de las credenciales, fomenta muy malos hábitos. Cuando añadimos más complejidad a las credenciales, hace más difícil recordar las contraseñas y refuerza las tentaciones de reutilizar las mismas credenciales en múltiples sitios, lo cual es una muy mala idea (ver regla #5). Puedes crear una gran contraseña sin hacerla súper complicada.
Regla #3: No cambies las contraseñas sólo por el hecho de cambiarlas

Cambiar las contraseñas para cambiarlas refuerza un par de malos hábitos. Principalmente, fomenta la selección de malas contraseñas (como las contraseñas que siguen las estaciones, como Verano2018 o Invierno2018). También fomenta la reutilización de las credenciales, de modo que, por ejemplo, cuando se pide a los usuarios que cambien su contraseña, es más fácil usar algo que ya se está usando en otro lugar. Esto es una mala idea (ver regla #5).

Deberías cambiar tu contraseña si sabes o sospechas que la cuenta ha sido comprometida. Hay un par de sitios en Internet que pueden ayudarte a encontrar esa información, como el sitio web ¿He sido "pwned"?.
Regla #4: No uses el nombre del sitio, aplicación o cosa como parte de la contraseña

Aunque incorporar el nombre del sitio o la aplicación en el proceso de creación de la contraseña puede ser tentador, no es una gran idea. Este concepto se extiende a los productos o servicios que el sitio o la aplicación también proporciona.  Cuando se crean credenciales, éstas deben ser únicas e independientes de la actividad en la que se participa.  Un ejemplo es si tu contraseña en Wikipedia es 'i edit wikipedia', por favor cambia tu contraseña inmediatamente.
Regla #5: No reutilices las contraseñas

Esta regla ha sido mencionada en casi todas las demás reglas porque es extremadamente importante.  Muchos de nosotros vamos a muchos lugares en Internet, y eso resulta en muchas credenciales. Eso también significa que no es súper raro crear credenciales comunes, reutilizadas a través de los medios sociales o la banca u otros sitios. A menudo hemos creado una "buena" contraseña fuerte que usamos para sitios sensibles, y una contraseña "ok" que se usa para cosas menos críticas.

Desafortunadamente, reciclar contraseñas es bastante peligroso. Aquí hay un escenario muy común y muy escuchado:

    Uno de tus sitios favoritos se ve comprometido. Es uno en el que usaste tu "buena" contraseña.
    Un vertedero de identificaciones de usuario y contraseñas de ese compromiso se publica en algún lugar de Internet.
    Los atacantes usan la información de la lista, incluyendo tu nombre de usuario y contraseña, para intentar entrar en otras cuentas de otros sitios.
    De repente, no es sólo la cuenta que está comprometida, sino también su banco y cualquier otro sitio sensible en el que haya utilizado esas credenciales.




Es totalmente justo decir en respuesta que no puedes recordar tantas contraseñas. Ciertamente no puedo. Por eso te animo a que uses un gestor de contraseñas, que almacena de forma segura todas tus contraseñas. Hay muchas opciones ahí fuera, tanto gratuitas como de pago. Algunos ejemplos son "lastpass", "keepass" y "sticky password".[3]

Por supuesto, sigue estas reglas al crear la contraseña de tu administrador de contraseñas: sólo usa una contraseña fuerte, única y larga. ¡Esta es la única contraseña que tendrás que recordar!
Regla #6: Las contraseñas son "ok". Un segundo factor es mejor!

La autenticación de dos factores, a menudo acortada a 2FA, es una forma de asegurar sus cuentas de tal manera que un usuario tiene que presentar dos pruebas antes de iniciar la sesión. Lo más frecuente es que se trate de una contraseña y un código temporal.

En este momento, la Fundación Wikimedia ofrece la autenticación de dos factores (2FA) sólo a las cuentas con ciertos roles privilegiados, aunque estamos explorando las opciones de 2FA para todos los usuarios.

Dicho esto, esta regla aún es buena para tener en cuenta mientras negocias tu camino en Internet. Algunos ejemplos de servicios de 2FA que puedes usar son Google Authenticator, YubiKey, o Authy.[3]
¿Qué hay de la nueva política de contraseñas?

Wikipedia no es inmune a ser blanco de ataques de contraseñas. Por eso estamos implementando una nueva política de contraseñas, que entrará en vigor a principios de 2019 para las cuentas recién creadas. Aunque los usuarios actuales no se verán afectados por este cambio, recomendamos encarecidamente a todos que revisen y sigan las reglas anteriores para mantener su cuenta segura. Si su contraseña no está a la altura, por favor, proponga algo nuevo.

La nueva política de contraseñas evaluará las nuevas credenciales en función de una lista de contraseñas conocidas comprometidas, débiles o simplemente deficientes en general, y aplicará una contraseña de ocho caracteres como mínimo para cualquier cuenta de nueva creación. Lo mismo se aplica a las cuentas privilegiadas (Administradores, administradores de bot, burócratas, usuarios de cheques, y otros), pero impondrá un mínimo de diez caracteres.

Puedes encontrar más información sobre estos cambios en MediaWiki.org.

Relacionado, pero por separado, el equipo de Seguridad de la Fundación Wikimedia también comenzará a realizar pruebas de contraseña regularmente. Estas pruebas buscarán las contraseñas débiles existentes, y animaremos a todo el mundo a proteger su cuenta utilizando una credencial sólida.

El equipo de seguridad se compromete a realizar una sensibilización regular sobre la seguridad, por lo que pronto verán más contenido como este. Gracias por ser un defensor de la seguridad de las cuentas.

John Bennett, Director de Seguridad
Fundación Wikimedia

- • • •
Notas a pie de página

    El Instituto Nacional de Estándares y Tecnología creía que la solución era crear contraseñas complejas para cada sitio web, pero cuanto más complejas se volvían las cosas, más difícil era cumplir los requisitos y recordar las contraseñas. Inadvertidamente, las recomendaciones del instituto fomentaban malos hábitos de credenciales como las contraseñas en notas post-it, o tener una sola contraseña "fuerte" y una que se utiliza para todo lo demás.
    Por una letanía de razones que no voy a entrar aquí, la palabra 'contraseña' está un poco anticuada. En adelante, "frase de contraseña" es realmente una mejor manera de pensar en todo esto. Sin embargo, me gustaría mantener las cosas simples, así que hemos usado "contraseña" para este post.
    Estos son ejemplos y no están respaldados por la Fundación Wikimedia.

[^artículo-original]: _Estamos cambiando nuestras políticas de contraseñas. Hay seis reglas para seleccionar una buena contraseña._
Fundación Wikimedia (en inglés). https://wikimediafoundation.org/news/2019/02/25/were-changing-our-password-policies-here-are-six-rules-for-selecting-a-good-password/


## Gestión de contraseñas

Es bastante habitual usar la misma contraseña en todos los sitios en los que tenemos una cuenta. Sin embargo, esto puede acarrear problemas, ya que cada vez nos registramos en más y más sitios, y es bastante frecuente que se produzcan brechas de seguridad y nuestras credenciales se vean comprometidas, en cuyo caso no sería muy difícil para un atacante hacer pruebas automatizadas en los sitios web más habituales con estas credenciales, teniendo una alta probabilidad de conseguir acceso con muchas de ellas.

Tenemos una capacidad limitada para recordar un gran número de datos no relacionados entre sí, y las contraseñas deberían ser justamente eso para ser eficientes. También está el problema de la fortaleza de las contraseñas, ya que una contraseña demasiado corta o con un juego de caracteres demasiado reducido es susceptible de ser adivinada mediante ataques de diccionario o de fuerza bruta.

Por eso es recomendable usar contraseñas diferentes para cada sitio en el que nos registremos. La forma de hacer esto de forma práctica es tener un gestor de contraseñas, al cual accedamos mediante una contraseña maestra. Evidentemente, aquí el riesgo viene de que nuestro archivo cifrado de contraseñas caiga en manos ajenas al mismo tiempo que la contraseña maestra, que deberemos recordar, sea demasiado débil. Pero es mucho más improbable que un filtrado masivo de las características antes descritas.

Lo más habitual es usar KeePass o cualquiera de sus clones o derivados. KeePass define un formato de archivo estándar y abierto (con extensión .kdbx) compatible con múltiples aplicaciones para todos los sistemas operativos de escritorio y móvil. Este archivo es una base de datos cifrada que puede contener una gran variedad de metadatos sobre cada sitio web donde tengamos una cuenta, no sólo el usuario y la contraseña. Y también nos permite guardar cualquier otro secreto de similar naturaleza, como información de nuestras tarjetas de crédito.
