---
title: Interfaces alternativas para redes sociales
discussion: https://gitlab.com/tinfoilismo/tinfoilismo.gitlab.io/-/issues/55
etiquetas:
  - eje-apertura
  - eje-privacidad-seguridad
aliases:
  - /docs/guías/interfaces-alternativas
  - /wiki/interfaces-alternativas/
# enlaces rotos a "efecto-red" en lineas 27, 38 y 44
---

Hoy en día, en internet hay un montón de recursos creados por las propias personas (_crowdsourcing_) que consideramos tan **público** que incluso los docentes empiezan a depender y apoyarse en ellos de manera orgánica. Mucho del contenido formativo o divulgador que encontramos en YouTube, las fotografías y contenido artístico gráfico en Instagram o la información y opiniones que encontramos en Twitter tienen un valor indiscutible, pero están **cautivos** en plataformas centralizadas controladas por empresas privadas (con la notable excepción de Wikipedia).

Si quieres acceder al contenido custodiado por estas empresas sin pasar por el aro y usar sus webs, sus aplicaciones o sus sistemas operativos, que espían toda la actividad que realizas y tratan de engancharte, o quieres conservar algún contenido que forma parte de tu vida o del patrimonio cultural, aquí encontrarás una solución.


## Servicios y aplicaciones que protegen tu privacidad

Existe un cada vez más creciente número de herramientas de **código libre** que nos permiten acceder a esta información pero sin entregar nuestra privacidad.

Estas herramientas pueden ser o bien aplicaciones que funcionan en tu propio dispositivo, o bien servicios web a los que accedes mediante un navegador. Se les denomina _alternative frontends_ en inglés (interfaces alternativas).

Incluso, nos ofrecen características que esas empresas nos niegan o, por ejemplo, vampirizan con suscripciones de pago para engrosar su modelo de negocio.


## Lee Twitter desde donde quieras

[Nitter](https://github.com/zedeus/nitter) es una plataforma que te permite acceder al contenido de Twitter desde un navegador web sin anuncios o seguir los perfiles que desees por [RSS](https://es.wikipedia.org/wiki/Fuente_web), una tecnología para leer noticias que Twitter soportaba en un principio, pero que eliminó para [favorecer su negocio](efecto-red).

La instancia oficial es https://nitter.net/, pero puedes utilizar cualquiera de los [proveedores alternativos](https://github.com/zedeus/nitter/wiki/Instances).

Para que tu navegador redirija automáticamente los enlaces a Nitter, puedes usar [estas extensiones del navegador](https://github.com/zedeus/nitter/wiki/Extensions). [Invidition](https://codeberg.org/Booteille/Invidition) en Firefox ([instalar aquí](https://addons.mozilla.org/en-US/firefox/addon/invidition/)) te permite utilizar, además, Invidious (leer abajo).


## Mira Instagram como quieras

### Desde un navegador web (Bibliogram)

[Bibliogram](https://github.com/cloudrac3r/bibliogram) es una plataforma que te permite acceder al contenido de Instagram desde un navegador web sin anuncios o seguir los perfiles por [RSS](https://es.wikipedia.org/wiki/Fuente_web), una tecnología para leer contenido periódico [sin encerrarte en una plataforma concreta](efecto-red).

La instancia oficial es https://bibliogram.art/, pero puedes utilizar cualquiera de [proveedores alternativos](https://github.com/cloudrac3r/bibliogram/wiki/Instances).

### Desde un sistema Android (Barinsta)

[Barinsta](https://barinsta.austinhuang.me) (antes InstaGrabber) es una aplicación para Android para acceder a Instagram que te permite ver las fotos y publicaciones, seguir cuentas y descargar fotos y otro contenido. Si [necesitas estar en Instagram](efecto-red), puedes identificarte con tu cuenta e interactuar normalmente en la plataforma, excepto para subir fotos. Adicionalmente, reemplaza algunas [características diseñadas para ser adictivas](efecto-red), como el _me gusta_ dando un doble toque, por [otras más conscientes](https://barinsta.austinhuang.me/en/latest/addictions/).

Puedes descargarla desde [F-Droid](https://f-droid.org/en/packages/me.austinhuang.instagrabber/). No está disponible en Play Store.



## Accede a YouTube&hellip; sin Google

### Desde un navegador web (Invidious)

[Invidious](https://github.com/omarroth/invidious) es una plataforma que permite acceder a YouTube, gestionar suscripciones y descargar vídeos sin anuncios. No tiene anuncios, tiene un modo oscuro opcional y, si quieres, carga los comentarios de Reddit en lugar de los de YouTube. Adicionalmente, no necesita JavaScript para funcionar.

La instancia oficial es https://invidio.us, pero puedes utilizar cualquiera de los [proveedores alternativos](https://github.com/omarroth/invidious/wiki/Invidious-Instances).

Para que tu navegador redirija automáticamente los enlaces y cargue los vídeos embebidos desde Invidious, puedes utilizar [estas extensiones del navegador](https://github.com/omarroth/invidious/wiki/Extensions).


### Desde un sistema Android (NewPipe)

[NewPipe](https://newpipe.schabi.org/) es una aplicación para Android para acceder a YouTube que te permite seguir cuentas, gestionar suscripciones y descargar vídeos sin anuncios. También funciona en sistemas Android personalizados sin servicios de Google, como LineageOS o Fairphone Open. Adicionalmente, te permite acceder a contenido de SoundCloud o MediaCCC.

Puedes descargarla desde [F-Droid](https://f-droid.org/es/packages/org.schabi.newpipe/). No está disponible en Play Store porque Google no lo permite.

También existen otras aplicaciones como
[SkyTube](https://skytube-app.com/) ([F-Droid](https://f-droid.org/es/packages/free.rm.skytube.oss/)),
[WebTube](https://github.com/martykan/webTube) ([F-Droid](https://f-droid.org/es/packages/cz.martykan.webtube/)) o
[YouTube Stream](https://github.com/thiolliere/YouTubeStream) ([F-Droid](https://f-droid.org/es/packages/org.thiolliere.youtubestream/)).

### Desde un ordenador GNU/Linux, Mac o Windows (FreeTube)

[FreeTube](https://freetubeapp.io/) es un programa para GNU/Linux, Mac o Windows para acceder a YouTube que permite gestionar suscripciones sin anuncios.

Puedes [descargarla desde su página web](https://freetubeapp.io/#download).


### Música bajo tus reglas en Android (MusicPiped)

Si eres de los que utiliza YouTube para escuchar música, la aplicación de código libre [MusicPiped](https://github.com/deep-gaurav/MusicPiped), para Android, te ofrece un reproductor enfocado a este propósito.

Puedes conseguirla en [F-Droid](https://f-droid.org/es/packages/deep.ryd.rydplayer/). Sorprendentemente, también está disponible en Play Store, pero probablemente Google la expulse como hizo con NewPipe.

