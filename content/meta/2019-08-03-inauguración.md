---
title: Inauguración
date: 2019-08-03
---

¡Tinfoilismo ha evolucionado! Inauguramos hoy la página web de esta base de conocimiento.

<!--more-->

Este proyecto empezó como una [wiki en GitLab](https://docs.gitlab.com/ee/user/project/wiki/), que editábamos desde la interfaz web. Hace unos días, convertí el contenido de la wiki a una página web estática con Hugo, utilizando un [interesante tema](https://themes.gohugo.io/hugo-book/) que muestra un botón de editar en cada página.

Una vez publicado con [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), solo quedaba una cosa que hacer. Así que compré el dominio `tinfoilismo.online` en Gandi.net. La elección no es arbitraria: Gandi [no se publicita](https://www.gandi.net/es/no-bullshit), está sujeta a la regulación europea y [apoya proyectos alternativos](https://www.gandi.net/es/gandi-supports) o de software libre económicamente.

Espero que este cambio marque una nueva era para Tinfoilismo.

¡Bienvenidos!
