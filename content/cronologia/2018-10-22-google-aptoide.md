---
headless: true
resumen: Google Aptoide
fecha: 2018-10-22
temas:
  - sanciones
sujetos:
  - google
---

Aptoide gana una batalla legal contra **Google** porque eliminó de manera subrepticia la aplicación de Aptoide de los dispositivos de los consumidores. [Reuters](https://uk.reuters.com/article/us-google-antitrust-aptoide/aptoide-wins-court-battle-against-google-in-landmark-case-idUKKCN1MW2CL)
