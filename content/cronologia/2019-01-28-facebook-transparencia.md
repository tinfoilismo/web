---
headless: true
resumen: Facebook transparencia
fecha: 2019-01-28
temas:
  - privacidad
  - transparencia
sujetos:
  - facebook
---

**Facebook** bloquea las herramientas de transparencia de anuncios de Mozilla, ProPublica y Who Targets Me, que investigan y revelan la segmentación de anuncios, en especial los de índole política. Argumenta que lo hace en cumplimiento estricto de sus políticas para proteger a los usuarios. [ProPublica](https://www.propublica.org/article/facebook-blocks-ad-transparency-tools)
