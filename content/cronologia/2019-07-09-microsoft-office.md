---
headless: true
resumen: Microsoft Office
fecha: 2019-07-09
temas:
  - privacidad
  - sanciones
sujetos:
  - microsoft
---

El responsable de la protección de datos de Hesse, Alemania, concluye que la suite en la nube **Microsoft** Office 365 no es apta para utilizarse en las escuelas porque su configuración estándar no cumple con el [RGPD]({{< relref rgpd >}}) y no «garantiza la soberanía digital del procesamiento de datos del Estado». [Heise.de](https://www.heise.de/newsticker/meldung/Datenschuetzer-Einsatz-von-Microsoft-Office-365-an-Schulen-ist-unzulaessig-4466156.html)
