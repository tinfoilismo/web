---
headless: true
resumen: Multa Google Android
fecha: 2018-07-18
temas:
  - sanciones
sujetos:
  - google
---

La Comisión Europea sanciona a **Google** con 4&thinsp;340 millones de € por prácticas monopolísticas referentes a Android y la certificación de los servicios de Google Play: obligó a los fabricantes a incluir el buscador de Google y el navegador Google Chrome, pagó a ciertos fabricantes y operadores para incluir exclusivamente su buscador e impidió a fabricantes vender dispositivos con versiones alternativas de Android. [Comisión Europea](http://europa.eu/rapid/press-release_IP-18-4581_es.htm)

  - **19 de marzo de 2019**. Google anuncia que dará opción a los usuarios de Android en Europa para elegir su navegador y buscador web, repitiendo la historia de Microsoft en 2009. [Blog corporativo de Google](https://www.blog.google/around-the-globe/google-europe/supporting-choice-and-competition-europe/)
