---
headless: true
resumen: Netflix desanonimización
fecha: 2008-03-07
temas:
  - privacidad
sujetos:
  - netflix
---

**Netflix** publicó un conjunto de datos no anonimizados correctamente con actividad real de sus usuarios para una competición (el [Netflix Prize](https://en.wikipedia.org/wiki/Netflix_Prize)) y la universidad de Texas en Austin, Estados Unidos publica un paper explicando el fiasco. [Universidad Cornell](https://www.cs.cornell.edu/~shmat/netflix-faq.html)
