---
headless: true
resumen: Facebook VPN
fecha: 2019-01-29
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** paga a usuarios (entre ellos, adolescentes) para que instalen un VPN y un certificado raíz de DNS en sus teléfonos y analizar así toda su actividad digital saltándose las limitaciones para empresas de Apple. [TechCrunch](https://techcrunch.com/2019/01/29/facebook-project-atlas/)

  - **30 de enero**. Apple reacciona retirándole el certificado para aplicaciones internas a Facebook. [The Verge](https://www.theverge.com/2019/1/30/18203551/apple-facebook-blocked-internal-ios-apps)
  - Sumario: [EFF](https://www.eff.org/deeplinks/2019/01/what-we-should-learn-facebook-research)
