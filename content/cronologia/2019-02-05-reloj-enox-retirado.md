---
headless: true
resumen: Reloj ENOX retirado
fecha: 2019-02-05
temas:
  - seguridad
sujetos:
  - enox
---

La Comisión Europea retira el _smartwatch_ **ENOX** Safe-KID-One, dirigido a niños, por múltiples riesgos contra la seguridad de datos privados. Las comunicaciones con el servidor no se cifran, el servidor permite acceder sin autenticación a los datos, como el historial de localizaciones GPS y números de teléfono, y un actor malintencionado podría incluso hablar con el niño usuario. [ZDNet](https://www.zdnet.com/article/eu-orders-recall-of-childrens-smartwatch-over-severe-privacy-concerns/) &middot; [Comisión Europea](https://ec.europa.eu/consumers/consumers_safety/safety_products/rapex/alerts/?event=viewProduct&reference=A12/0157/19&lng=en)
