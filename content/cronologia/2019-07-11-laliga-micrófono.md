---
headless: true
resumen: LaLiga micrófono
fecha: 2019-07-11
temas:
  - privacidad
  - sanciones
sujetos:
  - laliga
---

La AEPD española multa a **LaLiga** con 250.000&thinsp;€ por violar el [RGPD]({{< relref rgpd >}}) al usar el micrófono de los teléfonos de los usuarios de su app para detectar bares con fútbol pirateado sin ser transparente de cuándo lo hace (artículo 5.1) y tampoco facilitar adecuadamente al usuario la retirada de consentimiento. [eldiario.es](https://www.eldiario.es/tecnologia/Agencia-Proteccion-Datos-Liga-microfono_0_908859408.html)
