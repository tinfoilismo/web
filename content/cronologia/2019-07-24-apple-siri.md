---
headless: true
resumen: Apple Siri
fecha: 2019-07-24
temas:
  - privacidad
sujetos:
  - apple
---

Trabajadores externos de **Apple** escuchan grabaciones privadas de sus asistentes de voz (Apple HomPod) y la aplicación Siri, que se almacenan hasta por dos años. [El País](https://elpais.com/tecnologia/2019/07/23/actualidad/1563902000_568286.html) &middot; [The Guardian](https://www.theguardian.com/technology/2019/jul/26/apple-contractors-regularly-hear-confidential-details-on-siri-recordings)

 - **2 de agosto**. Apple suspende el programa de transcripciones y promete una actualización de software que permitirá a los usuarios elegir si sus grabaciones se incluyen en el programa. [TechCrunch](https://techcrunch.com/2019/08/01/apple-suspends-siri-response-grading-in-response-to-privacy-concerns/)
