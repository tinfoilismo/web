---
headless: true
resumen: YouTube desinformación
fecha: 2019-04-16
temas:
  - algoritmos
sujetos:
  - google
---

El sistema de **YouTube** contra la desinformación muestra un panel con información sobre los atentados del 11 de septiembre en EE.&thinsp;UU. en los vídeos del incendio de la catedral parisina de Notre Dame. [BuzzFeed](https://www.buzzfeednews.com/article/ryanhatesthis/youtube-notre-dame-fire-livestreams) &middot; [Genbeta](https://www.genbeta.com/actualidad/algoritmo-youtube-confunde-incendio-notre-dame-atentados-a-torres-gemelas)
