---
headless: true
resumen: Filtración Zoom
fecha: 2020-04-01
temas:
  - privacidad
  - seguridad
sujetos:
  - zoom
---

La aplicación de videoconferencias **Zoom** filtra la fotografía y cuenta de correo electrónico de los usuarios que se registran con servicios de correo poco convencionales, o de cuya existencia Zoom no sabe, porque trata las cuentas de correo como corporativas por defecto y las añade a un listado compartido entre ellas automáticamente. [VICE Motherboard](https://www.vice.com/en_us/article/k7e95m/zoom-leaking-email-addresses-photos)
