---
headless: true
resumen: Google El jueves
fecha: 2019-06-28
temas:
  - libertad de expresión
sujetos:
  - google
---

**Google** considera material pornográfico las portadas de la revista satírica «El jueves». Como consecuencia, la revista no podrá leerse desde la plataforma de Google ni desde la aplicación para Android. [Genbeta](https://www.genbeta.com/actualidad/google-censura-portadas-jueves-considera-material-pornografico-no-se-podra-leer-revista)
