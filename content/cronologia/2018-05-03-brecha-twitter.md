---
headless: true
resumen: Brecha Twitter
fecha: 2018-05-03
temas:
  - seguridad
sujetos:
  - twitter
---

Twitter descubre que estaba almacenando las contraseñas de más de 300 millones de cuentas sin cifrar y en texto plano, y urge a todos los usuarios a cambiar sus contraseñas. [Blog corporativo de Twitter](https://blog.twitter.com/en_us/topics/company/2018/keeping-your-account-secure.html) &middot; [Blog del periodista de seguridad Brian Krebs](https://krebsonsecurity.com/2018/05/twitter-to-all-users-change-your-password-now/)
