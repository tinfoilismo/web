---
headless: true
resumen: Facebook abandona reconocimiento facial
fecha: 2021-11-02
temas:
  - algoritmos
  - privacidad
sujetos:
  - facebook
---
Tras una demanda colectiva en Estados Unidos por infringir las leyes que regulan la privacidad biométrica, **Facebook** dejará de utilizar reconocimiento facial en las fotografías subidas a su red social, que identificaba a las personas incluso en las fotos subidas por terceras personas, y borrará gradualmente los datos biométricos de mil millones de personas. [The Verge](https://www.theverge.com/2021/11/2/22759613/meta-facebook-face-recognition-automatic-tagging-feature-shutdown?scrolla=5eb6d68b7fedc32c19ef33b4) · [EFF](https://www.eff.org/deeplinks/2021/11/face-recognition-so-toxic-facebook-dumping-it) · [ElOtroLado](https://www.elotrolado.net/noticias/internet/facebook-elimina-reconocimiento-facial-borrara-datos)
