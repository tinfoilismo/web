---
headless: true
resumen: Facebook monitorización
fecha: 2019-02-14
temas:
  - privacidad
sujetos:
  - facebook
---

**Facebook** mantiene una lista interna de _usuarios peligrosos_ calificados a su propia discreción e incluso rastrea el paradero de estos individuos pinchando la información de localización de las aplicaciones instaladas y sitios web visitados de Facebook en sus dispositivos sin su consentimiento ni conocimiento. [CNBC](https://www.cnbc.com/2019/02/14/facebooks-security-team-tracks-posts-location-for-bolo-threat-list.html)
