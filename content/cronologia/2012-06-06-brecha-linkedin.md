---
headless: true
resumen: Becha LinkedIn
fecha: 2021-06-06
temas:
  - seguridad
sujetos:
  - linkedin
---

Filtran cerca de 6,5 millones de contraeñas de usuarios de **LinkedIn**. Las contraseñas estaban débilmente cifradas. [TechCrunch](https://techcrunch.com/2012/06/06/6-5-million-linkedin-passwords-reportedly-leaked-linkedin-is-looking-into-it/) &middot; [Comunicado oficial de LinkedIn](https://blog.linkedin.com/2012/06/06/linkedin-member-passwords-compromised)
