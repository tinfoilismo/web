---
headless: true
resumen: Facebook teléfonos
fecha: 2019-09-05
temas:
  - privacidad
  - seguridad
sujetos:
  - facebook
---

Se encuentra una base de datos sin protección que almacena los números de teléfono asociados a 419 millones de cuentas de usuario de **Facebook**. Algunos de los registros también tenían el nombre del usuario, el sexo y la ubicación por país. [TechCrunch](https://techcrunch.com/2019/09/04/facebook-phone-numbers-exposed/)
