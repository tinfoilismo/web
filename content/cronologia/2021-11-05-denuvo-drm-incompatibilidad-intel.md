---
headless: true
resumen: Denuvo DRM incompatibilidad Intel
fecha: 2021-11-05
temas:
  - defectuoso por diseño
  - propiedad intelectual
---
Los sistemas de DRM como el de **Denuvo** causan que alrededor de cuarenta videojuegos de PC dejen de funcionar con los nuevos chips Alder Lake del fabricante de procesadores Intel. [ElOtroLado](https://www.elotrolado.net/noticias/tecnologia/chips-intel-alder-lake-juegos-problemas-drm) · [PCMag](https://www.pcmag.com/news/intel-these-50-plus-pc-games-are-incompatible-with-alder-lake-cpus-due) · [Intel](https://www.intel.com/content/www/us/en/support/articles/000088261/processors/intel-core-processors.html)
