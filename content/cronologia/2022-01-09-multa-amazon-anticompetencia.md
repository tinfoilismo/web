---
headless: true
resumen: multa Amazon anticompetencia
fecha: 2022-01-09
temas:
  - monopolio
  - sanciones
sujetos:
  - amazon
---
La Autoridad de Competencia y Mercado de Italia impone a **Amazon** una sanción récord de 1 128 millones de € porque la compañía abusó de su posición dominante absoluta en el mercado italiano para perjudicar a los competidores logísticos de comercio electrónico. Beneficia la visibilidad de los productos de los vendedores que utilizan su propio sistema logístico frente a los que eligen otros proveedores, que además deben pasar un estricto sistema de medición de rendimiento. [El País](https://elpais.com/economia/2021-12-09/italia-multa-con-mas-de-1100-millones-de-euros-a-amazon-por-perjudicar-la-competencia-en-el-comercio-electronico.html) · [El Otro Lado](https://www.elotrolado.net/noticias/internet/italia-amazon-multa-record)