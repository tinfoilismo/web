---
headless: true
resumen: Brecha Microsoft
fecha: 2019-04-14
temas:
  - seguridad
sujetos:
  - microsoft
---

Durante seis meses, un grupo de criminales tuvo acceso a correos electrónicos de usuarios no empresariales de Hotmail, MSN y Outlook tras conseguir acceso a la cuenta de un trabajador de soporte de Microsoft. [VICE Motherboard](https://motherboard.vice.com/en_us/article/ywyz3x/hackers-could-read-your-hotmail-msn-outlook-microsoft-customer-support) &middot; [ArtTechnica](https://arstechnica.com/gadgets/2019/04/hackers-could-read-non-corporate-outlook-comhotmail-for-six-months/)
