---
headless: true
resumen: Brecha Chile
fecha: 2019-08-01
temas:
  - privacidad
  - seguridad
---

Se filtra una base de datos sin protección con información electoral de 2017 del 80% de la población de Chile. La base de datos contenía nombres, domicilio, sexo, edad y número de identificación tributaria (RUT, o Rol Único Tributario) de 14&thinsp;308&thinsp;151 personas. [WizCase](https://www.wizcase.com/blog/chile-leak-research/) &middot; [ZDNet](https://www.zdnet.com/article/voter-records-for-80-of-chiles-population-left-exposed-online/)
