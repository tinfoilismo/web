---
headless: true
resumen: Microsoft Office
fecha: 2018-11-13
temas:
  - privacidad
sujetos:
  - microsoft
---

El Ministerio de Justicia y Seguridad neerlandés concluye que la suite en la nube de **Microsoft** Office 365 contraviene el [RGPD]({{< relref rgpd >}}) porque los datos sobre la actividad de sus usuarios, entre los que se incluyen empleados públicos del Estado, se almacena y procesa en los Estados Unidos, lo que «supone un alto riesgo para la privacidad de los usuarios». [nrc.nl](https://www.nrc.nl/nieuws/2018/11/13/microsoft-lekt-data-nederlandse-ambtenaren-naar-vs-a2755066)
