---
headless: true
resumen: Google Nest
fecha: 2019-02-06
temas:
  - privacidad
sujetos:
  - google
---

El sistema de alarma Nest Secure de **Google** tenía un micrófono que no figuraba en las especificaciones técnicas desde el principio y que se ha revelado exclusivamente tras una actualización que puede convertir el sistema en un asistente de voz. [TechaPeek](https://www.techapeek.com/2019/02/06/nest-secure-had-a-secret-microphone-can-now-be-a-google-assistant/) &middot; [Business Insider](https://www.businessinsider.es/nest-microphone-was-never-supposed-to-be-a-secret-2019-2)
