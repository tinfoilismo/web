---
headless: true
resumen: Brecha Apple FaceTime
fecha: 2019-01-28
temas:
  - seguridad
sujetos:
  - apple
---

Se descubre un error en la aplicación de videollamadas de Apple, FaceTime, que permite escuchar el audio de la persona contactada antes de que descuelgue. [BuzzFeed](https://www.buzzfeednews.com/article/nicolenguyen/facetime-bug-iphone)

 - El mismo día, Apple reacciona rápidamente desactivando la característica de grupos hasta que lo resuelvan. [9to5Mac](https://9to5mac.com/2019/01/28/facetime-calling-bug-fix/)
