---
headless: true
resumen: Google oculta brecha
fecha: 2019-10-08
temas:
  - transparencia
  - seguridad
sujetos:
  - google
---

**Google** ocultó un error en Google+ activo desde 2015 que exponía información de perfil (como nombre, género, correo electrónico, ocupación y edad) de 500.000 usuarios a desarrolladores, supuestamente en un intento de evitar repercusiones legales. [WSJ](https://www.wsj.com/articles/google-exposed-user-data-feared-repercussions-of-disclosing-to-public-1539017194) (muro de pago) &middot; [The Verge](https://www.theverge.com/2018/10/8/17951914/google-plus-data-breach-exposed-user-profile-information-privacy-not-disclosed)
