---
headless: true
resumen: Facebook Messenger audio
fecha: 2019-08-13
temas:
  - privacidad
sujetos:
  - facebook
---

Trabajadores externos de **Facebook** escuchan y transcriben grabaciones privadas de chats de audio en Facebook Messenger para los usuarios que activan esa opción. Facebook afirma que canceló el programa de transcripciones tras los recientes escándalos de Apple y Amazon y afirma que las transcripciones no se utilizaron para mejorar los anuncios o los algoritmos de personalización. [Bloomberg](https://www.bloomberg.com/news/articles/2019-08-13/facebook-paid-hundreds-of-contractors-to-transcribe-users-audio)
