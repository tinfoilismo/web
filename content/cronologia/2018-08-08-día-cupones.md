---
headless: true
resumen: Día cupones
fecha: 2018-08-08
temas:
  - privacidad
sujetos:
  - día
---

Los supermercados **Día** en España reparten unos cupones descuento que, de ser utilizados, pretenden dar por hecho que el usuario concede permiso para compartir sus datos con terceras empresas y recibir publicidad, violando así el [RGPD]({{< relref rgpd >}}). [El Confidencial](https://www.elconfidencial.com/tecnologia/2018-08-08/dia-cupones-descuento-proteccion-datos_1602548/)
