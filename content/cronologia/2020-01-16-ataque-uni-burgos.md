---
headless: true
resumen: Ataque uni Burgos
fecha: 2020-01-16
temas:
  - privacidad
  - seguridad
sujetos:
  - universidad burgos
---

Los datos personales de 6&thinsp;800 usuarios de la Universidad de Burgos (UBU) fueron afectados por un ciberataque el 14 de enero. No se han exfiltrado contraseñas ni información bancaria. [eldiario.es](https://www.eldiario.es/cyl/sociedad/Universidad-Burgos-ciberataque-personales-usuarios_0_985502266.html)
