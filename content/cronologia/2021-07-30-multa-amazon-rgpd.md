---
headless: true
resumen: Multa Amazon RGPD
fecha: 2021-07-30
temas:
  - privacidad
  - sanciones
sujetos:
  - amazon
---
La Comisión Nacional de Protección de Datos de Luxemburgo sanciona a la multinacional **Amazon** con una multa de 746 millones de € por considerar que su tratamiento de los datos personales no cumple con la normativa de protección de datos europea ([RGPD]({{< relref rgpd >}})). [elDiario.es](https://www.eldiario.es/tecnologia/multa-record-746-millones-amazon-violar-proteccion-datos-ue_1_8185942.html)
