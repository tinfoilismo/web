---
headless: true
resumen: YouTube Beethoven Wagner
fecha: 2018-09-03
temas:
  - algoritmos
  - propiedad intelectual
sujetos:
  - google
---

El sistema Content ID de **YouTube** retira obras de Beethoven y Wagner de un profesor de música.
[TorrentFreak](https://torrentfreak.com/youtube-targets-music-profs-public-domain-beethoven-and-wagner-uploads-180903/)
