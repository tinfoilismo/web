---
headless: true
resumen: Multa Google Shopping
fecha: 2017-06-27
temas:
  - sanciones
sujetos:
  - google
---
La Comisión Europea sanciona a **Google** con 2&thinsp;420 millones de € por abuso de posición dominante como motor de búsqueda y su servicio Google Shopping de comparación de productos: colocó sistemáticamente en los resultados de búsqueda los resultados de su servicio en posiciones destacadas y «enterró» los de la competencia. [Comisión Europea](http://europa.eu/rapid/press-release_IP-17-1784_es.htm) &middot; [The Verge](https://www.theverge.com/2017/6/27/15872354/google-eu-fine-antitrust-shopping) &middot; [BBC](https://www.bbc.co.uk/news/technology-40406542)

- **10 de noviembre de 2021**. El Tribunal General de la Unión Europea confirma la multa. [ElOtroLado](https://www.elotrolado.net/noticias/internet/google-apelacion-pierde-multa-antimonopolio)