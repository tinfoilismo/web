---
headless: true
resumen: Brecha Android TV
fecha: 2019-03-04
temas:
  - seguridad
sujetos:
  - google
---

Un error en la aplicación de Google Home para Android TV revela los perfiles de otros usuarios. Google reacciona desactivando la característica de visualización de fotos con Google Photos. [XDA-Developers](https://www.xda-developers.com/bug-exposes-google-photos-android-tv/)
