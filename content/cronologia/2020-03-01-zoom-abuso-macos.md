---
headless: true
resumen: Zoom abuso macOS
fecha: 2020-03-01
temas:
  - dark patterns
  - seguridad
sujetos:
  - zoom
---

Un investigador de ciberseguridad descubre que el instalador de **Zoom**, la aplicación de videoconferencias, elude los mecanismos de seguridad del sistema operativo MacOS mediante el abuso de una característica diseñada para evaluar si la aplicación puede ser instalada. En un caso incluso pide permisos de administración haciéndose pasar por el sistema, prácticas comúnmente utilizadas por software malicioso. [Blog de Felix Seele](https://www.vmray.com/cyber-security-blog/zoom-macos-installer-analysis-good-apps-behaving-badly/)

  - **2 de abril.** Zoom actualiza la aplicación eliminando todas las técnicas controvertidas y ajustándose a los estándares de instaladores de Mac OS. [Twitter de Felix Seele](https://twitter.com/c1truz_/status/1245767226499764225?s=21)
