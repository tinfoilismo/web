---
headless: true
resumen: Google Android
fecha: 2018-11-27
temas:
  - privacidad
  - dark patterns
sujetos:
  - google
---

**Google** utiliza diferentes patrones engañosos de diseño, información confusa u oculta y molesta insistencia para que los usuarios de Google Android acepten ser monitorizados geográficamente, violando la legislación vigente ([RGPD]({{< relref rgpd >}})). [Forbrukerrådet (agencia noruega de consumo)](https://www.forbrukerradet.no/side/google-manipulates-users-into-constant-tracking)
