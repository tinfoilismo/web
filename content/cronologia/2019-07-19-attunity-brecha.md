---
headless: true
resumen: Attunity brecha
fecha: 2019-07-19
temas:
  - seguridad
sujetos:
  - attunity
---

Al menos un terabyte de copias de seguridad de **Attunity**&mdash; un proveedor de servicios de la mitad del Fortune 100&mdash; se encontraban accesibles por el público en una serie de instancias en la nube de Amazon S3. Almacenaban copias de seguridad de las cuentas de OneDrive de los empleados y entre los documentos expuestos se incluyen documentos internos de las empresas. [Upguard](https://www.upguard.com/breaches/attunity-data-leak)
