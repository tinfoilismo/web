---
headless: true
resumen: Facebook contraseña correo-e
fecha: 2019-04-03
temas:
  - privacidad
  - seguridad
  - dark patterns
sujetos:
  - facebook
# enlace roto a "términos y condiciones"
---

**Facebook** pide la contraseña del correo electrónico a nuevos usuarios que se registran en su red social para verificar su dirección de correo, un método propio de ataques de _phising_. Otros métodos de verificación estaban escondidos tras un [patrón oscuro de diseño](términos-y-condiciones). [The Hacker News](https://thehackernews.com/2019/04/facebook-email-password.html) &middot; [Daily Beast](https://www.thedailybeast.com/beyond-sketchy-facebook-demanding-some-new-users-email-passwords) &middot; [EFF](https://www.eff.org/deeplinks/2019/04/facebook-got-caught-phishing-friends)
