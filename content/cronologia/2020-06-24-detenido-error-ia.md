---
headless: true
resumen: Detenido error IA
fecha: 2020-06-24
temas:
  - algoritmos
  - transparencia
sujetos:
  - dataworks-plus
---

Un sistema de reconocimiento facial de **DataWorks Plus** identificó erróneamente a un hombre negro en Estados Unidos, causando su posterior detención sin haber cometido delito alguno. El sistema de videovigilancia con esta tecnología le confundió con otra persona. [The New York Times](https://www.nytimes.com/2020/06/24/technology/facial-recognition-arrest.html)
