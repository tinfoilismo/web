---
headless: true
resumen: Facebook compartición
fecha: 2018-12-18
temas:
  - privacidad
sujetos:
  - facebook
  - amazon
  - microsoft
  - yahoo
  - spotify
  - netflix
  - huawei
  - sony
  - apple
---

Desde 2010, **Facebook** tiene acuerdos para compartir información privada de sus usuarios (información de contacto, lista de amigos e incluso mensajes privados) con 150 empresas, entre las que figuran Amazon, Microsoft, Yahoo, Spotify y Netflix y fabricantes como Huawei, Sony y Apple. [The New York Times](https://www.nytimes.com/es/2018/12/19/facebook-privacidad/) &middot; [The Verge](https://www.theverge.com/2018/12/18/18147616/facebook-user-data-giveaway-nyt-apple-amazon-spotify-netflix)
