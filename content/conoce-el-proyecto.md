---
title: Conoce el proyecto
---

Tinfoilismo es una base de conocimiento colaborativa por personas preocupadas por las derivas antihumanas de la tecnología y el mundo digital. Queremos recopilar recursos para la defensa de los derechos digitales y el ejercicio de la libertad, así como guías de elección y autoalojamiento de servicios, guías de mínimos datos y cronologías de sucesos contrarios a la democracia, a los derechos humanos y a la ética cívica.

## Entidades con objetivos afines

- Electronic Frontier Foundation (EFF). Estados Unidos. https://www.eff.org/
- Eticas Foundation. España. https://eticasfoundation.org/
- European Digital Rights (EDRi). Europa. https://edri.org/
- Maldita Tecnología. España. https://maldita.es/malditatecnologia/
- Plataforma en Defensa de la Libertad de Información (PDLI). España. http://libertadinformacion.cc/
- Privacy International. Reino Unido. https://www.privacyinternational.org/
- Tactical Technology Collective. Alemania. https://tacticaltech.org/
- Tracking.exposed. https://tracking.exposed/
- Xnet. España. https://xnet-x.net/

(orden alfabético)

## Recursos de similar naturaleza

- [_Anonimízate: manual de autodefensa electrónica_](https://www.eticasconsulting.com/anonimizate-manual-de-autodefensa-electronica/). Eticas Foundation, 2014.
- [_Data Detox Kit: kit de desintoxicación de datos_](https://datadetox.myshadow.org/). Tactical Tech, 2017.
- [_Informe de la salud de internet_](https://internethealthreport.org/). Mozilla, anual desde 2018.
- [_Soberanía tecnológica, vol. 2_](https://sobtec.gitbooks.io/sobtec2/content/es/). Coordinado por Alex Haché, 2018.
