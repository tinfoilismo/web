---
title: Descentralización
---

El mundo digital es más saludable cuando son muchos actores los que lo controlan mediante consensos, no pocos actores de gran tamaño que lo dominan.
