---
title: Privacidad y seguridad
---

Para ser nosotros mismos en el mundo digital necesitamos confiar en los sistemas que nos protejan como personas.
